#version 330 core
#define PI 3.14159265359
smooth in vec2 vert_position;
out vec4 color;

uniform mat4 light_projection;
uniform vec3 global_light_position;
uniform vec3 camera_position;

uniform sampler2D position_tex;
uniform sampler2D normal_tex;
uniform sampler2D albedo_spec_tex;
uniform sampler2D shadow_map_tex;

// Credits to learnopengl.com
float shadow_calculation(vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadow_map_tex, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
    float bias = 0.0001;
    float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;

    return shadow;
}

// Credits to wikipedia
vec3 linear_blinn_phong(vec3 normal, vec3 world_position, vec3 camera_pos, vec3 light_position, vec3 diffuse_color) {
  const vec3 light_color = vec3(1.0,1.0,1.0);
  const vec3 specular_color = vec3(1.0,1.0,1.0);
  const float specular_intensity = 1.0;

  vec3 view_dir = normalize(camera_pos - world_position);

  float distance = length(light_position - world_position); //pow(length(light_dir), 2);

  vec3 light_dir = normalize(light_position - world_position);

  vec3 diffuse = max(dot(normal, light_dir), 0) * diffuse_color * light_color;

  // Blinn-Phong
  vec3 half_dir = normalize(light_dir + view_dir);
  float spec = pow(max(dot(half_dir, normal), 0.0), 16.0);

  vec3 specular = light_color * spec * specular_intensity;

  float attenuation =  10.0 / (1.0 + 0.7 * distance + 1.8 * distance * distance);
  diffuse *= attenuation;
  specular *= attenuation;

  return diffuse_color * 0.7 + diffuse + specular;
}

void main() {
  vec3 world_position = texture(position_tex, vert_position).rgb;
  vec4 albedo_spec = texture(albedo_spec_tex, vert_position);
  float shadow = shadow_calculation( light_projection * vec4(world_position,1.0));
  vec3 normal = texture(normal_tex, vert_position).xyz;

  vec3 diffuse = albedo_spec.rgb;
  float specular = albedo_spec.a;

  float f = shadow * length(world_position) * length(camera_position) * length(normal) * length(albedo_spec.xyz) * 0.0000000001;

  color.a = 1.0;

  vec3 sun_pos = global_light_position * 0.1;
  color.rgb = linear_blinn_phong(normal, world_position, camera_position, sun_pos, diffuse) + f;

  // const float screen_gamma = 2.2;
  // vec3 color_gamma_corrected = pow(color_linear, vec3(1.0/screen_gamma));
  color.rgb  *= 0.5 * (1.0 - shadow) + 0.5;
}

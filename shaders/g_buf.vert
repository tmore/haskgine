#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

out vec2 vert_uv;
out vec3 vert_normal;
out vec3 vert_tangent;
out vec3 vert_bitangent;
smooth out vec4 vert_position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(){

  vert_tangent = tangent;
  vert_bitangent = bitangent;

  vert_uv = uv;
  vert_normal = normal;
  vec4 pos_t = vec4(vertex, 1.0);
  vert_position = model * pos_t;
 
  //gl_Position = projection * view * model * pos_t;
  gl_Position = projection * view * model * pos_t;
}

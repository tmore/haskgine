#version 420 core
layout (location = 0) out vec3 tex_position;
layout (location = 1) out vec3 tex_normal;
layout (location = 2) out vec4 tex_albedo_spec;
//layout (location = 3) out vec3 tex_tangent;

in vec2 vert_uv;
in vec3 vert_normal;
//in vec3 vert_tangent;
in vec3 vert_bitangent;
smooth in vec4 vert_position;

out vec4 color;

uniform sampler2D diffuse;
uniform sampler2D normal_map;
uniform sampler2D specular;

uniform vec3 col;
uniform int hasTexture;

void main() {
  //mat3 tangent_space_mat = mat3(vert_tangent, vert_bitangent, vert_normal);
  //tex_tangent = vert_tangent * 0.5 + 0.5;
  tex_position = vert_position.xyz;

  vec3 normal = texture(normal_map, vert_uv).xyz;
  tex_albedo_spec = texture(diffuse, vert_uv);

  //  if (tex_albedo_spec.a == 0.) discard;
  tex_albedo_spec.a = texture(specular, vert_uv).r;

  tex_position = vert_position.xyz;
  tex_normal = vert_normal.rgb;
}

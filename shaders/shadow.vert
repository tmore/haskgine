#version 330 core
layout (location = 0) in vec3 vertex;

uniform mat4 light_projection;
uniform mat4 model_trans;

void main(){
  gl_Position = light_projection * model_trans * vec4(vertex, 1.0);
}

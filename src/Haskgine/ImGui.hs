
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE  KindSignatures #-}

{-# LANGUAGE  StandaloneDeriving #-}

module Haskgine.ImGui where

import Reflex
import Reflex.Host.Class

import Data.IORef

import Control.Monad.Ref

import qualified DearImGui

import GHC.Exts ( toList )

import Data.List 

import Data.Sequence
import Control.Monad.State.Strict

class MonadReflexImGuiHost t (m :: * -> *) | m -> t where
  getImGuiData :: m ImGuiData
  setImGuiData :: ImGuiData -> m ()

newtype ImGuiData =
  ImGuiData
  { _currentTreeNode :: IORef ImGuiTree }

newtype ImGuiReflexT t (m :: * -> *) a =
  ImGuiReflexT { unImGuiReflexT :: StateT ImGuiData m a }
  deriving (Functor, Applicative, Monad )

--deriving instance (Functor m)        => Functor (ImGuiReflexT m)
--deriving instance (Applicative m)    => Applicative (ImGuiReflexT m)
--deriving instance (Monad m)          => Monad (ImGuiReflexT m)
deriving instance (MonadFix m)       => MonadFix (ImGuiReflexT t m)
--deriving instance (MonadIO m)        => MonadIO (ImGuiReflexT m)
--deriving instance                       MonadTrans ImGuiReflexT
--deriving instance (ReflexHost t, MonadError m)     => MonadError e (ReflexHaskgineT r t m)
deriving instance (ReflexHost t, TriggerEvent t m) => TriggerEvent t (ImGuiReflexT t m)


instance (Monad m) => MonadReflexImGuiHost t (ImGuiReflexT t m) where
  getImGuiData = ImGuiReflexT get
  setImGuiData val = ImGuiReflexT (put val)

instance MonadIO m => MonadIO (ImGuiReflexT t m) where
  liftIO = ImGuiReflexT . liftIO

instance (ReflexHost t, MonadRef m) => MonadRef (ImGuiReflexT t m) where
    type Ref (ImGuiReflexT t m) = Ref m

    newRef     r   = lift $ newRef     r
    readRef    r   = lift $ readRef    r
    writeRef   r x = lift $ writeRef   r x
    modifyRef  r f = lift $ modifyRef  r f
    modifyRef' r f = lift $ modifyRef' r f
    
instance MonadTrans (ImGuiReflexT t) where
  lift = ImGuiReflexT . lift

instance (Reflex t, PostBuild t m, ReflexHost t, Monad m) => PostBuild t (ImGuiReflexT t m) where
  getPostBuild = lift getPostBuild

instance (Reflex t, MonadReflexCreateTrigger t m) => MonadReflexCreateTrigger t (ImGuiReflexT t m) where
  newEventWithTrigger = lift . newEventWithTrigger
  newFanEventWithTrigger initializer = lift $ newFanEventWithTrigger initializer

instance (ReflexHost t, PerformEvent t m) => PerformEvent t (ImGuiReflexT t m) where
  type Performable (ImGuiReflexT t m) = ImGuiReflexT t (Performable m)
  performEvent_ event = do
    imguiData <- getImGuiData
    lift $ performEvent_ $ fmap (fmap fst . flip runStateT imguiData . unImGuiReflexT) event
  performEvent event = do
    imguiData <- getImGuiData 
    lift $ performEvent $ fmap (fmap fst . flip runStateT imguiData . unImGuiReflexT) event

instance (Adjustable t m, Monad m, MonadIO m) => Adjustable t (ImGuiReflexT t m) where
  runWithReplace ma evt = withSubtree $ do
    origState@ImGuiData{..} <- getImGuiData
    let evt' = fmap (\mb -> do
                      liftIO $ modifyIORef' _currentTreeNode (const mempty)
                      runStateT (unImGuiReflexT mb) origState
                  )
                  evt
        subWithReplace = runWithReplace (runStateT (unImGuiReflexT ma) origState) evt'
        res = fmap (\ ((x , _) , e ) -> (x , fmap fst e )) subWithReplace
    lift res
  traverseDMapWithKeyWithAdjust kvma dMapKV evt = do
    origState@ImGuiData{..} <- getImGuiData
    lift $ traverseDMapWithKeyWithAdjust (\ka v ->
                                             fmap fst (runStateT (unImGuiReflexT (kvma ka v)) origState) ) dMapKV evt

  traverseDMapWithKeyWithAdjustWithMove kvma dMapKV evt = do
    origState@ImGuiData{..} <- getImGuiData
    lift $ traverseDMapWithKeyWithAdjustWithMove (\ka v ->
                                             fmap fst (runStateT (unImGuiReflexT (kvma ka v)) origState) ) dMapKV evt

  traverseIntMapWithKeyWithAdjust f im evt = do
    origState@ImGuiData{..} <- getImGuiData
    let foo = traverseIntMapWithKeyWithAdjust (\ka v -> fmap fst (runStateT (unImGuiReflexT (f ka v)) origState) ) im evt
    lift foo

instance ( ReflexHost t
         , Applicative m
         , Monad m
         , MonadSample t m
         ) => MonadSample t (ImGuiReflexT t m) where
  sample = lift . sample

instance (ReflexHost t, MonadHold t m) => MonadHold t (ImGuiReflexT t m) where
  hold a = lift . hold a
  holdDyn a = lift . holdDyn a
  holdIncremental p = lift . holdIncremental p
  buildDynamic ma = lift . buildDynamic ma
  headE = lift . headE


newImGuiDataRoot :: MonadIO m => m ImGuiData
newImGuiDataRoot = do
  ref <- liftIO $ newIORef mempty
  pure $ ImGuiData ref

runImGuiReflexT :: (Adjustable t m, Monad m, MonadIO m) => ImGuiReflexT t m a -> ImGuiData -> m (a, ImGuiTree )
runImGuiReflexT ma ref =
  fmap fst $ flip runStateT ref $ unImGuiReflexT inner
  where
    inner = do
      res <- ma
      finalTree <- liftIO $ readIORef (_currentTreeNode ref)
      pure (res , finalTree)
      
data ImGuiNode = SubTree (IORef ImGuiTree)
               | Widget (String , IO ()) -- String only for debug

type ImGuiTree = Seq ImGuiNode

debugShowImGuiNode :: MonadIO m => ImGuiNode -> m String
debugShowImGuiNode (SubTree ioref) = 
  debugShowImGuiTree =<< liftIO (readIORef ioref)
debugShowImGuiNode (Widget (str, _ )) =
  pure $ show str



debugShowImGuiTree :: MonadIO m => ImGuiTree -> m String
debugShowImGuiTree seq = do
  res <- mapM debugShowImGuiNode seq
  pure $ "(" ++ intercalate "," (toList res) ++ ")"
 
runImGuiTree :: (Monad m, MonadIO m) => ImGuiTree -> m ()
runImGuiTree tree =
  forM_ tree $ \case
     SubTree t -> do
       subTree <- liftIO $ readIORef t
       runImGuiTree subTree
     Widget (_, ma) -> liftIO ma


insertWidgetAtPoint :: (MonadIO m, MonadReflexImGuiHost t m) => IO () -> m ()
insertWidgetAtPoint ma = do
  ImGuiData{..} <- getImGuiData
  liftIO $ modifyIORef' _currentTreeNode (|>Widget ("", ma) )
  pure ()

insertWidgetAtPointWithMsg :: (MonadIO m, MonadReflexImGuiHost t m) => String -> IO () -> m ()
insertWidgetAtPointWithMsg msg ma = do
  ImGuiData{..} <- getImGuiData
  liftIO $ modifyIORef' _currentTreeNode (|>Widget (msg, ma))
  pure ()

insertSubTreeAtPoint :: (MonadIO m, MonadReflexImGuiHost t m) => m (IORef ImGuiTree)
insertSubTreeAtPoint = do
  ref <- liftIO $ newIORef mempty
  ImGuiData{..} <- getImGuiData
  liftIO $ modifyIORef' _currentTreeNode (|>SubTree ref )
  pure ref

withSubtree :: (MonadIO m, MonadReflexImGuiHost t m) => m a -> m a
withSubtree ma = do
  ref <- insertSubTreeAtPoint
  ImGuiData{..} <- getImGuiData
  let oldRef = _currentTreeNode
  setImGuiData (ImGuiData ref)
  res <- ma
  setImGuiData (ImGuiData oldRef)
  pure res

withWindow :: (MonadIO m, MonadReflexImGuiHost t m) => String -> m a -> m a
withWindow name ma = do
  insertWidgetAtPointWithMsg "begin" (void $ DearImGui.begin name)
  res <- ma
  insertWidgetAtPointWithMsg "End" DearImGui.end
  pure res

text :: (MonadIO m, MonadReflexImGuiHost t m) => String -> m ()
text str = insertWidgetAtPointWithMsg ("Text: " ++ str) (liftIO (DearImGui.text str))

button :: (MonadIO m, TriggerEvent t m, MonadReflexImGuiHost t m, MonadReflexCreateTrigger t m, MonadRef m, MonadRef m,  Ref m ~ Ref IO) => String -> m (Event t ())
button label = do
  (event, clickTrigger) <- newEventWithTriggerRef
  ( event , triggerEvt ) <- newTriggerEvent
  insertWidgetAtPointWithMsg
    ("Button: " ++ label)
    ( do
        isClicked <- DearImGui.button label
        if isClicked
          then triggerEvt ()
          else pure ()
    )
  pure event

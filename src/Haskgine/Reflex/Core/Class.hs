-- | Author: Tomas Möre 2019, Inpsired by reflex-sdl2

{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE UndecidableInstances   #-}
{-# LANGUAGE RankNTypes   #-}

module Haskgine.Reflex.Core.Class where

import Control.Monad.Reader
import Control.Monad.Trans  (lift)
import Reflex               (Event, Reflex)

import Haskgine.Reflex.Core.Internal

import qualified Graphics.UI.GLFW as GLFW
-- import           Reflex.DynamicWriter.Base (DynamicWriterT)

class (Reflex t, Monad m) => HasHaskgineCore r t m | m -> t r where
  askWindow :: m GLFW.Window
  askRenderer :: m r

  askScreenChangeEvent :: m (Event t (Int, Int))
  askKeyboardEvent :: m (Event t (GLFW.Key, Int, GLFW.KeyState, GLFW.ModifierKeys))
  askCharEvent :: m (Event t Char)
  askMouseMoveEvent :: m (Event t (Double, Double))
  askPostRenderEvent :: m (Event t ())


-- instance HasHaskgineCore r t m => HasHaskgineCore r t (ReaderT a m) where
--   askWindow = lift askWindow
--   askRenderer = lift askRenderer

--   askScreenChangeEvent = lift askScreenChangeEvent
--   askKeyboardEvent = lift askKeyboardEvent
--   askMouseMoveEvent = lift askMouseMoveEvent
--   askPostRenderEvent = lift askPostRenderEvent


instance (Reflex t, Monad m, MonadReader (HaskgineCoreData r t) m) => HasHaskgineCore r t m where
  askWindow = asks _window
  askRenderer = asks _renderer
  askScreenChangeEvent = asks _screenChangeEvent
  askKeyboardEvent = asks _keyboardEvent
  askCharEvent = asks _charEvent
  askMouseMoveEvent = asks _mouseMoveEvent
  askPostRenderEvent = asks _postRenderEvent

-- | Author: Tomas Möre 2019

{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TupleSections              #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE DataKinds #-}

module Haskgine.Reflex.Core.Host
  (  host
  , ReflexHaskgine
  , RenderControlls(..)
  , ShadowRenderSettings(..)
  , GL.GLint
  , ConcreteReflexHaskgine
  ) where

import qualified Haskgine.Scene as Scene

import Haskgine.Renderer (Renderer, DeferredRenderer, ShadowRenderSettings(..))
import qualified Haskgine.Renderer as Renderer

import qualified Haskgine.Renderer.ShadowRenderer (ShadowRenderSettings(..))
import qualified Haskgine.Renderer.ShadowRenderer as SR

import Haskgine.Graphics.Camera

import Haskgine.Reflex.Core.Base
import Haskgine.Reflex.Core.Class
import Haskgine.Reflex.Core.Internal

import Control.Monad.Fix         (MonadFix)
import Control.Monad.IO.Class    (liftIO, MonadIO)
import Control.Concurrent        (newChan, readChan)
import Control.Concurrent.MVar
import Control.Monad.STM
import Control.Concurrent.STM.TQueue
import Control.Concurrent.Async  (async, cancel)
import Control.Monad.Ref (readRef)
import Control.Monad             (forM_, unless, void)
import Control.Monad.Identity    (Identity (..))

import Reflex
import Reflex.Host.Class
import Reflex.NotReady.Class

import DearImGui
import DearImGui.GLFW
import DearImGui.OpenGL3
import DearImGui.GLFW.OpenGL

import Haskgine.ImGui

import Control.Exception

-- import Data.Dependent.Sum (DSum ((:=>)))
import Data.IORef
import Data.Dependent.Sum (DSum((:=>)))
import Data.Maybe

import Data.Default

import qualified Graphics.UI.GLFW as GLFW
import qualified Graphics.Rendering.OpenGL as GL

import Control.Monad.Reader


import Numeric.LinearAlgebra.Static hiding ((<>), tr)
import Numeric.LinearAlgebra ((!))
import Data.StateVar (($=!))

-- | A collection of constraints that represent the default reflex-haskgine network.
type ReflexHaskgine r t (m :: * -> *) = ( Renderer r
                                         , Reflex t
                                         , MonadHold t m
                                         , MonadSample t m
                                         , Adjustable t m
                                         , PostBuild t m
                                         , PerformEvent t m
                                         , TriggerEvent t m
                                         , MonadFix m
                                         , MonadIO m
                                         , MonadIO (Performable m)
                                         , MonadIO (PushM t)
                                         , MonadReader (HaskgineCoreData r t) m
                                         , Scene.HasScene m
                                         , ReflexHost t
                                         , NotReady t m
                                         )

-- Idea inspired by Reflex SDL2
type ConcreteReflexHaskgine = ReflexHaskgineT DeferredRenderer Spider (ImGuiReflexT Spider (TriggerEventT Spider (PostBuildT Spider (PerformEventT Spider (SpiderHost Global)))))


data RenderControlls t = MKRenderControlls
                       { _camera     :: Behavior t Camera
                       , _clearColor :: Behavior t (R 3)
                       , _framebufferSize :: Behavior t (GL.GLint,GL.GLint)
                       , _runHostLoop  :: Behavior t Bool

                       , _shadowRenderSettings :: Behavior t (Maybe ShadowRenderSettings)

                       }

instance Reflex t => Default (RenderControlls t) where
  def = MKRenderControlls
        { _camera     = constant def
        , _clearColor = constant (vec3 0 0 0)
        , _framebufferSize = constant (800, 600)
        , _runHostLoop = constant True
        , _shadowRenderSettings = constant (Just def)
        }

-- | The host method is the core of running the reflex network in the haskgine Context.
-- Todo: Make this method a bit more readable and split it into smaller pieces.
host :: GLFW.Window ->
        DeferredRenderer ->
        (ConcreteReflexHaskgine (RenderControlls Spider)) ->
        IO ()
host window renderer app = do
  imGuiContext <- DearImGui.createContext
  DearImGui.GLFW.OpenGL.glfwInitForOpenGL window False
  DearImGui.OpenGL3.openGL3Init

  runSpiderHost $ do
    (screenChangeEvent, screenChangeTriggerRef) <- newEventWithTriggerRef

    (keyboardEvent, keyboardTriggerRef) <- newEventWithTriggerRef
    (charEvent, charTriggerRef) <- newEventWithTriggerRef

    (mouseMoveEvent, mouseMoveTriggerRef) <- newEventWithTriggerRef

    (postRenderEvent, postRenderTiggerRef) <- newEventWithTriggerRef
    (postBuildEvent,  postBuildTriggerRef) <- newEventWithTriggerRef

    glfwQueue <- liftIO $ atomically newTQueue
    routeGLFWEvents window glfwQueue

    -- Channel which will be used as an intermediate for events that should be triggered between updates
    eventTriggerChan <- liftIO newChan
    -- MVar containing triggers that should be executed
    varTriggers <- liftIO $ newMVar []
    -- Secondary thread that handels the reading of the chan,
    assyncHandleTrig <- liftIO $ async $ fix $ \loop -> do
      trigs <- readChan eventTriggerChan
      modifyMVar_ varTriggers $ \ a -> pure (a <> trigs)
      loop

    scene <- Scene.newScene
    let coreData = MKHaskgineCoreData
                   { _window = window
                   , _renderer = renderer
                   , _scene = scene

                   , _screenChangeEvent = screenChangeEvent
                   , _charEvent = charEvent
                   , _keyboardEvent = keyboardEvent
                   , _mouseMoveEvent =  mouseMoveEvent

                   , _postRenderEvent = postRenderEvent
                   }

    imguiTreeRoot <- newImGuiDataRoot
    let f1 = runReflexHaskgineT app coreData
        f2 = runImGuiReflexT f1 imguiTreeRoot

    ((renderControlls, imGuiTree ), FireCommand fire) <-
      hostPerformEventT $ flip runPostBuildT postBuildEvent
                        $ flip runTriggerEventT eventTriggerChan
                        $ flip runImGuiReflexT imguiTreeRoot
                        $ runReflexHaskgineT app coreData

    let whenTriggerable triggerRef f = do
              readRef triggerRef >>= \case
                Nothing -> pure ()
                Just tr -> void $ f tr

    -- Trigger the post build event.
    whenTriggerable postBuildTriggerRef $ \tr ->
      fire [tr :=> Identity ()] $ pure ()

    fix $ \loop ->  do
      cam <- sample (_camera renderControlls)

      clearColor <- sample $ (\ v-> extract (v / realToFrac ( norm_2 v))) <$> _clearColor renderControlls
      liftIO $ GL.clearColor $=! GL.Color4 (realToFrac $ clearColor ! 0)
                                            (realToFrac $ clearColor ! 1)
                                            (realToFrac $ clearColor ! 2)
                                            1
      liftIO $ GLFW.makeContextCurrent (Just window)
      (framebufferWidth, framebufferHeight) <- sample (_framebufferSize renderControlls)
      let renderTarget = def{ Renderer._viewportSize = GL.Size framebufferWidth framebufferHeight}

      shadowRenderSettings <- sample (_shadowRenderSettings renderControlls)

      Scene.withEntities scene $ \ entities ->
        Renderer.render renderer cam shadowRenderSettings renderTarget entities

      mPostRenderTigger <- readRef postRenderTiggerRef
      forM_ mPostRenderTigger $ \postRenderTigger -> do
        fire [postRenderTigger :=> Identity ()] $ pure ()

      liftIO GLFW.pollEvents

      liftIO (do DearImGui.OpenGL3.openGL3NewFrame
                 DearImGui.GLFW.glfwNewFrame
                 DearImGui.newFrame

                 runImGuiTree imGuiTree

                 DearImGui.render
                 DearImGui.OpenGL3.openGL3RenderDrawData =<< DearImGui.getDrawData
             )
      liftIO $ GLFW.swapBuffers window

      -- Handles all events given by glfw
      glfwEvent <- liftIO $ atomically (flushTQueue glfwQueue)
      forM_ glfwEvent $ \case
        ScreenChangeEvent dat ->
          whenTriggerable screenChangeTriggerRef $ \tr ->
              fire [tr :=> Identity dat] $ pure ()
        KeyboardEvent dat ->
          whenTriggerable keyboardTriggerRef $ \tr ->
              fire [tr :=> Identity dat] $ pure ()
        CharEvent dat ->
          whenTriggerable charTriggerRef $ \tr ->
              fire [tr :=> Identity dat] $ pure ()
        MouseMoveEvent dat ->
          whenTriggerable mouseMoveTriggerRef $ \tr ->
              fire [tr :=> Identity dat] $ pure ()
      -- Handles internally triggered events
      userTriggers <- liftIO $ swapMVar varTriggers []
      mes <- liftIO $ forM userTriggers $ \(EventTriggerRef ref :=> TriggerInvocation a _cb) ->
        fmap (\e -> e :=> Identity a) <$> readRef ref
      _ <- fire (catMaybes mes) $ pure ()
      forM_ userTriggers $ \(_ :=> TriggerInvocation _a cb) -> liftIO cb

      shouldQuit <- sample (_runHostLoop renderControlls)
      if not shouldQuit
        then  loop
        else do
        liftIO $ cancel assyncHandleTrig


-- | Naively routes all evnets int othe same TQeueu, in the future it may be possible to use
-- the reading detectring part of 'TriggerRefs' to  turn some reading on and off
routeGLFWEvents :: MonadIO m => GLFW.Window -> TQueue GLFWEvent -> m ()
routeGLFWEvents window queue = liftIO $ do
  GLFW.setFramebufferSizeCallback window $ Just $ \ _ width height -> do
    writeMsg (ScreenChangeEvent (width, height))
  GLFW.setKeyCallback window $ Just $ \ _ key scancode action mods -> do
    writeMsg $ KeyboardEvent (key, scancode, action, mods)
  GLFW.setCharCallback window $ Just $ \ _ char -> do
    writeMsg $ CharEvent char
  GLFW.setCursorPosCallback window $ Just $ \ _ x y -> do
    writeMsg $ MouseMoveEvent (x,y)

  where
    writeMsg = atomically . writeTQueue queue

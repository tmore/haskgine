-- | Author: Tomas Möre 2019
module Haskgine.Reflex.Core.Internal where

import qualified Graphics.UI.GLFW as GLFW

import Reflex

import Haskgine.Scene (Scene)

data HaskgineCoreData r t = MKHaskgineCoreData
                       { _window   :: GLFW.Window
                       , _renderer :: r
                       , _scene    :: Scene

                       -- EVENTS
                       , _screenChangeEvent :: Event t (Int, Int)
                       , _keyboardEvent     :: Event t (GLFW.Key, Int, GLFW.KeyState, GLFW.ModifierKeys)
                       , _charEvent         :: Event t Char
                       , _mouseMoveEvent    :: Event t (Double, Double)
                       , _postRenderEvent   :: Event t ()
                       }

data GLFWEvent = MouseMoveEvent (Double, Double)
               | KeyboardEvent (GLFW.Key, Int, GLFW.KeyState, GLFW.ModifierKeys)
               | ScreenChangeEvent (Int,Int)
               | CharEvent Char

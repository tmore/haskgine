{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures             #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE UndecidableInstances       #-}

-- | Author: Tomas Möre 2019
-- Code heavily inspired / copied from reflex-sdl2
module Haskgine.Reflex.Core.Base where



--import Control.Monad.Except (MonadError)
import Control.Monad.IO.Class   (MonadIO)

import Haskgine.ImGui

import Control.Monad.Reader
    ( MonadFix,
      MonadIO,
      MonadTrans(..),
      ReaderT(..),
      MonadReader,
      asks )
import Reflex
    ( Adjustable(..),
      MonadHold(headE, hold, holdDyn, holdIncremental, buildDynamic),
      MonadSample(..),
      Reflex,
      NotReady(..),
      PerformEvent(..),
      PostBuild(..),
      TriggerEvent )
import Reflex.Host.Class ( ReflexHost, MonadReflexCreateTrigger( .. ) )
import Reflex.NotReady.Class ( NotReady(..) )

import Haskgine.Scene ( HasScene(askScene) )

import Haskgine.Reflex.Core.Internal ( HaskgineCoreData(_scene) )

import Control.Monad.Ref

------------------------------------------------------------------------------
newtype ReflexHaskgineT renderer spider (m :: * -> *) extraData =
   ReflexHaskgineT { unReflexHaskgineT :: ReaderT (HaskgineCoreData renderer spider) m extraData }

-- newtype ReflexHaskgineT r t m a =
--   ReflexHaskgineT { unReflexHaskgine :: ReaderT (HaskgineCoreData r t) m a }

runReflexHaskgineT :: ReflexHaskgineT r t m a -> HaskgineCoreData r t -> m a
runReflexHaskgineT = runReaderT . unReflexHaskgineT


deriving instance (ReflexHost t, Functor m)        => Functor (ReflexHaskgineT r t m)
deriving instance (ReflexHost t, Applicative m)    => Applicative (ReflexHaskgineT r t m)
deriving instance (ReflexHost t, Monad m)          => Monad (ReflexHaskgineT r t m)
deriving instance (ReflexHost t, MonadFix m)       => MonadFix (ReflexHaskgineT r t m)
deriving instance (ReflexHost t, MonadIO m)        => MonadIO (ReflexHaskgineT r t m)
deriving instance                                     MonadTrans (ReflexHaskgineT r t)
deriving instance (ReflexHost t, Monad m)          => MonadReader (HaskgineCoreData r t) (ReflexHaskgineT r t m)
--deriving instance (ReflexHost t, MonadError m)     => MonadError e (ReflexHaskgineT r t m)
deriving instance (ReflexHost t, TriggerEvent t m) => TriggerEvent t (ReflexHaskgineT r t m)

instance (ReflexHost t, MonadRef m) => MonadRef (ReflexHaskgineT r t m) where
    type Ref (ReflexHaskgineT r t m) = Ref m

    newRef     r   = lift $ newRef     r
    readRef    r   = lift $ readRef    r
    writeRef   r x = lift $ writeRef   r x
    modifyRef  r f = lift $ modifyRef  r f
    modifyRef' r f = lift $ modifyRef' r f
    

-- | WARNING! This implementation is really shitty, Adjustable should most
-- probably be corrected as well,
instance (Monad m, ReflexHost t) => NotReady t (ReflexHaskgineT r t m) where
  notReadyUntil _ =  pure ()
  notReady = pure ()

------------------------------------------------------------------------------

instance (Reflex t, PostBuild t m, ReflexHost t, Monad m) => PostBuild t (ReflexHaskgineT r t m) where
  getPostBuild = lift getPostBuild

------------------------------------------------------------------------------
instance (ReflexHost t, PerformEvent t m) => PerformEvent t (ReflexHaskgineT r t m) where
  type Performable (ReflexHaskgineT r t m) = ReflexHaskgineT r t (Performable m)
  performEvent_ = ReflexHaskgineT . performEvent_ . fmap unReflexHaskgineT
  performEvent  = ReflexHaskgineT . performEvent  . fmap unReflexHaskgineT

------------------------------------------------------------------------------
instance ( Reflex t
         , ReflexHost t
         , Adjustable t m
         , Monad m
         ) => Adjustable t (ReflexHaskgineT r t m) where
  runWithReplace ma evmb =
    ReflexHaskgineT $ runWithReplace (unReflexHaskgineT ma) (unReflexHaskgineT <$> evmb)
  traverseDMapWithKeyWithAdjust kvma dMapKV = ReflexHaskgineT .
    traverseDMapWithKeyWithAdjust (\ka -> unReflexHaskgineT . kvma ka) dMapKV
  traverseDMapWithKeyWithAdjustWithMove kvma dMapKV = ReflexHaskgineT .
    traverseDMapWithKeyWithAdjustWithMove (\ka -> unReflexHaskgineT . kvma ka) dMapKV
  traverseIntMapWithKeyWithAdjust f im = ReflexHaskgineT .
    traverseIntMapWithKeyWithAdjust (\ka -> unReflexHaskgineT . f ka) im

------------------------------------------------------------------------------
instance ( ReflexHost t
         , Applicative m
         , Monad m
         , MonadSample t m
         ) => MonadSample t (ReflexHaskgineT r t m) where
  sample = lift . sample

------------------------------------------------------------------------------
instance (ReflexHost t, MonadHold t m) => MonadHold t (ReflexHaskgineT r t m) where
  hold a = lift . hold a
  holdDyn a = lift . holdDyn a
  holdIncremental p = lift . holdIncremental p
  buildDynamic ma = lift . buildDynamic ma
  headE = lift . headE


instance (ReflexHost t, MonadIO m) => HasScene (ReflexHaskgineT r t m) where
  askScene = asks _scene


instance (Monad m, ReflexHost t, MonadReflexImGuiHost t m) => MonadReflexImGuiHost t (ReflexHaskgineT r t m)  where
  getImGuiData = lift getImGuiData
  setImGuiData = lift . setImGuiData


instance (ReflexHost t, MonadReflexCreateTrigger t m) => MonadReflexCreateTrigger t (ReflexHaskgineT r t m) where
  newEventWithTrigger = lift . newEventWithTrigger
  newFanEventWithTrigger initializer = lift $ newFanEventWithTrigger initializer
  

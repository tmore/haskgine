-- | Author: Tomas Möre 2019
-- Module with diffrent ways to handle a camera, should be enough for basic generic needs

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}

module Haskgine.Reflex.Util.Camera where

import Haskgine.Reflex.Core
import Haskgine.Graphics.Camera

import Haskgine.Reflex.Util.Input.Keyboard ( specificKeyHeldBehavior )

import Graphics.UI.GLFW (Key(..))

import Haskgine.Util.Linalg

import Haskgine.Reflex.Util.Input.Mouse
import Data.Default
import Data.Align
import Data.These
import Data.Foldable ( foldlM )

applyWhen :: Bool -> (a -> a) -> a -> a
applyWhen True f a = f a
applyWhen False _ a = a

-- | Constructs a rotation behaviour for the camera that corresponds to FPS games
-- Note to self, doesn't work properly!
makeFPSCamera :: (ReflexHaskgine r t m) => Camera -> Event t Double -> m (Dynamic t Camera)
makeFPSCamera defaultCamera tickEvent = do
  mouseDelta <- getDeltaMouseMove

  w_heldB <- specificKeyHeldBehavior Key'W
  s_heldB <- specificKeyHeldBehavior Key'S
  a_heldB <- specificKeyHeldBehavior Key'A
  d_heldB <- specificKeyHeldBehavior Key'D
  let moveHeldStatuses = (,,,) <$> w_heldB <*> s_heldB <*> a_heldB <*> d_heldB
      anyMoveHeld = foldlM (\ acc beh -> fmap (acc||) beh) False [w_heldB, s_heldB,  a_heldB, d_heldB ]
      moveUpdateEvent = attach moveHeldStatuses (gate anyMoveHeld tickEvent)

  let rotate :: (Double, Double) -> Camera -> Camera
      rotate (dx,dy) cam = withCameraRotation cam (dy / 200) (dx / 200) 0
      move :: (Bool, Bool, Bool, Bool) -> Double -> Camera -> Camera
      move (forwardHeld, backdawdHeld, leftHeld, rightHeld) tickDelta cam@Camera{..} =
        let tickDeltaVec = vec3 tickDelta tickDelta tickDelta
            moveForward cam = if forwardHeld then tickDeltaVec * _camAt else 0
            moveBackward cam = if backdawdHeld then tickDeltaVec * negate _camAt else 0
            moveLeft cam = if leftHeld then tickDeltaVec * negate (cross _camAt _camUp) else 0
            moveRight cam = if rightHeld then tickDeltaVec * cross _camAt _camUp else 0
        in cam `updateCameraPosition` (+ (moveForward cam + moveBackward cam + moveLeft cam + moveRight cam))

  let updF :: These ((Bool, Bool, Bool, Bool) , Double) (Double , Double) -> Camera -> Camera
      updF (That d) = rotate d
      updF (This (heldKeys, tickDelta)) = move heldKeys tickDelta
      updF (These (heldKeys, tickDelta) d) = move heldKeys tickDelta . rotate d
  foldDyn ($) defaultCamera (alignWith updF moveUpdateEvent mouseDelta)

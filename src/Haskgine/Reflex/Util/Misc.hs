-- | Author: Tomas Möre 2019
{-# LANGUAGE RankNTypes #-}
-- This module contains misc utitlies for writing an app. Most methods here
-- should probably be put
module Haskgine.Reflex.Util.Misc where


import Reflex
import Control.Monad.Fix


-- | A function that simply counts the number of times the event has fired
eventCounter :: forall t m a . (Reflex t, MonadFix m, MonadHold t m) => Event t a -> m (Dynamic t Integer)
eventCounter = foldDyn (\ _ b -> b + 1) 0


-- -- | Folds over two events continously
-- biFoldDyn :: forall t m a b c. (Reflex t, MonadFix m, MonadHold t m) => (b -> a -> a) -> (c -> a -> a) -> a -> Event t b -> Event t c ->  m (Dynamic t Integer)
-- biFoldDyn f1 f2 acc e1 e2 = do

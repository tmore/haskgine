-- | Author: Tomas Möre 2019
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleContexts #-}


module Haskgine.Reflex.Util.Time where

import Haskgine.Reflex.Core

import Data.Time

secondsSinceStart :: (ReflexHaskgine r t m) =>  m (Event t Double)
secondsSinceStart = do
  timeNow <- liftIO $ getCurrentTime
  tick <- tickLossy 0.015 timeNow
  pure $ (\TickInfo{..} -> fromIntegral _tickInfo_n * 0.3 ) <$> tick


-- Event giving time number of seconds passed since last tick (as a double)
deltaTickEvent :: (ReflexHaskgine r t m) =>  m (Event t Double)
deltaTickEvent = do
  timeNow <- liftIO $ getCurrentTime
  let tickTarget = 0.0015
  tick <- tickLossy tickTarget timeNow
  dynTick <- holdDyn (TickInfo timeNow 0 0) tick
  let deltaFold = (\TickInfo{..} (prevUTC, prev_delta) -> (_tickInfo_lastUTC, realToFrac (diffUTCTime _tickInfo_lastUTC prevUTC)))
  delta <- foldDyn deltaFold (timeNow , 0 ) tick
  pure $ snd <$> updated delta

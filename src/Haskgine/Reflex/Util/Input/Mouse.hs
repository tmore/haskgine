-- | Author: Tomas Möre 2019
-- This module should contain some utility functions for handling mouse input in diffrent ways
{-# LANGUAGE FlexibleContexts, LambdaCase #-}

module Haskgine.Reflex.Util.Input.Mouse
  ( getDeltaMouseMove
  , handleCursor
  , handleCursorDisabled
  , CursorInputMode(..)) where

import Haskgine.Reflex.Core
import Graphics.UI.GLFW (CursorInputMode(..))
import qualified Graphics.UI.GLFW as GLFW

import Control.Monad (void)


-- | Construct an event stream of mouse movement deltas
getDeltaMouseMove :: (ReflexHaskgine r t m) => m (Event t (Double, Double))
getDeltaMouseMove = do
  mouseLoc <- askMouseMoveEvent
  t <- foldDyn (\ (x,y) ((prevX, prevY), _) ->
                  if (prevX, prevY) == (0, 0)
                  then ((x,y), (0, 0))
                  else ((x,y), (prevX - x, prevY - y))
                  ) ((0,0), (0,0)) mouseLoc
  pure $ updated $ fmap snd t

-- | Will toggle the visibility of the cursor
handleCursor :: (ReflexHaskgine r t m) => Dynamic t CursorInputMode -> m ()
handleCursor cursorStatus = do
  initialCursorStatus <- sample $ current $ cursorStatus
  window <- askWindow
  liftIO $ GLFW.setCursorInputMode window initialCursorStatus
  performEvent_ $ fmap (\ mode -> void $ liftIO $ GLFW.setCursorInputMode window mode)
                       (updated cursorStatus)


handleCursorDisabled :: (ReflexHaskgine r t m) => Dynamic t Bool -> m ()
handleCursorDisabled =
  handleCursor . fmap (\case
                          True -> CursorInputMode'Disabled
                          False -> CursorInputMode'Normal
                      )

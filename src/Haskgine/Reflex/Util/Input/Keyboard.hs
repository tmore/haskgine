-- | Author: Tomas Möre 2019
-- This module should contain some utility functions for handling the keyboard in diffrent ways.

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}

module Haskgine.Reflex.Util.Input.Keyboard
  (   Keyboard(..)
    , KeyInfo(..)
    , getKeyInfo
    , specificKeyEvent
    , specificKeyPressEvent
    , specificKeyReleaseEvent
    , specificKeyRepeatEvent
    , specificKeyHeldDyn
    , specificKeyHeldBehavior
    
    , specificKeyDyn
    , keyboardDyn
    , specificKeyToggleDyn

    , onCharCombo
    , onKeyCombo
    , NonEmpty(..)
    , Key(..)
    , ModifierKeys(..)
    , KeyState(..)
  ) where

import Haskgine.Reflex.Core

import qualified Graphics.UI.GLFW as GLFW
import Graphics.UI.GLFW (Key(..), ModifierKeys(..), KeyState(..))

import Data.Time

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HM

import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NonEmpty

data KeyInfo = MKKeyInfo
               { _key         :: Key
               , _timePressed :: UTCTime
               , _scanCode    :: Int
               , _modifiers   :: ModifierKeys
               } deriving (Show, Eq)

-- | Just to avoid sending the hasmap all over the place, in the furutre there
-- could be some other underlying datastrucutre, using it directly should probabily be avoided
newtype Keyboard = MKKeyboard { unKeyboard :: HashMap Int KeyInfo}

getKeyInfo :: Keyboard -> Key -> (Maybe KeyInfo)
getKeyInfo keyboard key = HM.lookup (fromEnum key) (unKeyboard keyboard)

-- | Crates an event what is filtered on a specific key
specificKeyEvent :: (ReflexHaskgine r t m) => Key -> m (Event t (Key, Int, KeyState, GLFW.ModifierKeys))
specificKeyEvent targetKey = do
  keyboardEvent <- askKeyboardEvent
  pure $ ffilter (\ (keyIn,_,_,_) -> keyIn == targetKey) keyboardEvent


-- | Gives the event
specificKeyStatusEvent :: (ReflexHaskgine r t m) => Key -> KeyState -> m (Event t (GLFW.Key, Int, GLFW.KeyState, GLFW.ModifierKeys))
specificKeyStatusEvent targetKey targetStatus =
  ffilter (\ (_,_,keyState,_) -> keyState == targetStatus) <$> specificKeyEvent targetKey


specificKeyPressEvent :: (ReflexHaskgine r t m) => Key -> m (Event t (GLFW.Key, Int, GLFW.KeyState, GLFW.ModifierKeys))
specificKeyPressEvent = flip specificKeyStatusEvent KeyState'Pressed

specificKeyReleaseEvent :: (ReflexHaskgine r t m) => Key -> m (Event t (GLFW.Key, Int, GLFW.KeyState, GLFW.ModifierKeys))
specificKeyReleaseEvent = flip specificKeyStatusEvent KeyState'Released


specificKeyRepeatEvent :: (ReflexHaskgine r t m) => Key -> m (Event t (GLFW.Key, Int, GLFW.KeyState, GLFW.ModifierKeys))
specificKeyRepeatEvent = flip specificKeyStatusEvent KeyState'Repeating

specificKeyHeldDyn :: (ReflexHaskgine r t m) => Key -> m (Dynamic t Bool)
specificKeyHeldDyn key = do
  keyEvent <- specificKeyEvent key
  foldDyn (\ (_,_,keyState,_) isHeld ->
              case keyState of
                KeyState'Pressed -> True
                KeyState'Repeating -> True
                KeyState'Released -> False
          ) False
    keyEvent
  
specificKeyHeldBehavior :: (ReflexHaskgine r t m) => Key -> m (Behavior t Bool)
specificKeyHeldBehavior key = do
  keyEvent <- specificKeyEvent key
  keyDyn <- foldDyn (\ (_,_,keyState,_) isHeld ->
                       case keyState of
                         KeyState'Pressed -> True
                         KeyState'Repeating -> True
                         KeyState'Released -> False
                       ) False
                    keyEvent
  return $ current keyDyn

-- | Gives a dynbamic of the current state of a specified key. Will not update on repeat
specificKeyDyn :: (ReflexHaskgine r t m) => Key -> m (Dynamic t (Maybe KeyInfo))
specificKeyDyn targetKey = do
  event <- specificKeyEvent targetKey
  foldDynMaybeM (\ (key, scanCode, keyState, modifiers) mKey ->
                   case (mKey, keyState) of
                     (Just _, GLFW.KeyState'Released) ->
                       pure $ Just Nothing
                     (Nothing, GLFW.KeyState'Pressed) -> do
                       t <- liftIO $ getCurrentTime
                       pure $ Just $ Just $ MKKeyInfo { _key = key
                                                      , _timePressed = t
                                                      , _scanCode = scanCode
                                                      , _modifiers = modifiers
                                                      }
                     _ -> pure $ Nothing
                   ) Nothing event

-- | Constructs an internal state that holds information about all keys that
-- currently are pressed. Can be usefull if information about many keys are
-- needed. Does not react on key repeats
keyboardDyn :: (ReflexHaskgine r t m) => m (Dynamic t Keyboard)
keyboardDyn = do
  keyboardEvent <- askKeyboardEvent
  foldDynMaybeM (\(key, scanCode, keyState, modifiers) MKKeyboard{ unKeyboard = hm} ->
             case keyState of
               GLFW.KeyState'Pressed -> do
                 t <- liftIO $ getCurrentTime
                 let kInfo = MKKeyInfo { _key = key
                                       , _timePressed = t
                                       , _scanCode = scanCode
                                       , _modifiers = modifiers
                                       }
                 pure $ Just (MKKeyboard $ HM.insert (fromEnum key) kInfo hm)
               GLFW.KeyState'Released ->
                 pure $ Just $ MKKeyboard $ HM.delete (fromEnum key) hm
               _ -> pure Nothing
                ) (MKKeyboard mempty) keyboardEvent

-- | Creates a toggelable dynamic using a speficied key, the dynamic defaults to
-- the provided key each time a 'key-press' event is recieved the value is flipped
specificKeyToggleDyn :: (ReflexHaskgine r t m) => Key -> Bool -> m (Dynamic t Bool)
specificKeyToggleDyn targetKey initBool =
  toggle initBool =<< specificKeyPressEvent targetKey


-- | Fires an event if a specific combination of keys has been pressed
onCharCombo :: (ReflexHaskgine r t m) => NonEmpty Char -> m (Event t ())
onCharCombo combo = do
  charEvent <- askCharEvent
  t <- foldDyn (\ c (_, (first:| rest)) ->
                      if | c == first && null rest -> (True, combo)
                         | c == first -> (False, head rest :| tail rest)
                         | otherwise -> (False, combo)
               ) (False, combo) charEvent
  (updated) <$> foldDynMaybe (\ (b,_) _ -> if b then Just () else Nothing) () (updated t)


-- | Fires an event if a specific combination of keys has been pressed
onKeyCombo :: (ReflexHaskgine r t m) => NonEmpty Key -> m (Event t ())
onKeyCombo combo = do
  keyEvent <- askKeyboardEvent
  t <- foldDyn (\ c (_, (first:| rest)) ->
                     if | c == first && null rest -> (True, combo)
                        | c == first -> (False, head rest :| tail rest)
                        | otherwise -> (False, combo)
               ) (False, combo) (fmap (\(k,_,_,_) -> k) keyEvent)
  (updated) <$> foldDynMaybe (\ (b,_) _ -> if b then Just () else Nothing) () (updated t)

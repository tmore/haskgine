-- | Author: Tomas Möre 2019
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module Haskgine.Reflex.Util.Entity
  ( newColoredUnitCube
  , newColoredUnitQuad
  , newColoredMesh
  , updateEntityOn
  ) where

import Haskgine.Reflex.Core

import Haskgine.Graphics.Entity.Entity

import Haskgine.Util.GL.BasicMeshes as Meshes
import Haskgine.Util.GL.TextureColor
import qualified Haskgine.Util.GL.VAO as VAO

import Haskgine.Scene

import Reflex
import Reflex.Host.Class

import Data.Time
import Data.Maybe
import Text.Printf

import qualified Data.HashMap.Strict as HM

import Data.Text (Text)
import qualified Data.Text as T
import Data.Default

import Control.Concurrent.MVar

toColorName :: (Real f) => (f,f,f) -> Text
toColorName (rf,gf,bf) = T.pack $ printf "##SIMPLE_COLOR-%f,%f,%f" r g b
  where
    (r,g,b) = (realToFrac rf, realToFrac gf, realToFrac bf) :: (Float,Float,Float)

newColoredMesh :: (ReflexHaskgine r t m, Real f) =>
               (f,f,f) ->
               Text ->
                m Mesh ->
               (Entity -> Entity) ->
               m (MVar Entity)
newColoredMesh color@(r,g,b) meshName createMesh initF = do
  let textureName = toColorName color
      baseEntityName = meshName <> textureName
  (key, varEntity) <- addEntity baseEntityName
                                [Recipe textureName (newColorTextureRGBF r g b)]
                                [Recipe meshName (do mesh <- createMesh
                                                     VAO.fromMesh mesh def)]
                                (\ textures vaos -> do
                                    let tex = HM.lookup textureName textures
                                        vao = fromJust $ HM.lookup meshName vaos
                                    pure $ Entity { _vao = vao, _albedoTexture = tex, _transform = def}
                                )
  liftIO $ modifyMVar_ varEntity (pure . initF)
  pure varEntity

newColoredUnitCube :: (ReflexHaskgine r t m, Real f) =>
               (f,f,f) ->
               (Entity -> Entity) ->
               m (MVar Entity)
newColoredUnitCube color initF =
  newColoredMesh color "##UNIT-CUBE" (pure Meshes.unitCube) initF

newColoredUnitQuad :: (ReflexHaskgine r t m, Real f) =>
                      (f,f,f) ->
                      (Entity -> Entity) ->
                      m (MVar Entity)
newColoredUnitQuad color initF = do
  newColoredMesh color "##UNIT-QUAD" (pure Meshes.unitQuad) initF

updateEntityOn :: (ReflexHaskgine r t m) =>
                  MVar Entity ->
                  Event t a ->
                  (a -> Entity -> Entity) ->
                  m (Dynamic t Entity)
updateEntityOn varEntity event f = do
  initial <- liftIO $ readMVar varEntity
  eventUpdate <- performEvent $ fmap
    (\ a ->
        liftIO $ modifyMVar varEntity (\ e -> let e' = f a e
                                              in pure (e', e'))
    ) event
  holdDyn initial eventUpdate

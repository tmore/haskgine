-- | Author: Tomas Möre 2019
-- This module contains standard code for how to update the window when the screen size changes
{-# LANGUAGE FlexibleContexts #-}
module Haskgine.Reflex.Util.Window where

import Haskgine.Reflex.Core

directlyUpdatedFramebuffer :: (ReflexHaskgine r t m) =>
                              GLint ->
                              GLint ->
                              m (Behavior t (GLint, GLint))
directlyUpdatedFramebuffer defaultWidth defaultHeight = do
  screenChange <- askScreenChangeEvent
  let corrected = (\ (x,y) -> (fromIntegral x, fromIntegral y)) <$> screenChange
  hold (defaultWidth, defaultHeight) corrected

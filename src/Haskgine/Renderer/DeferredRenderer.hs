{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, RecordWildCards, FlexibleContexts, ScopedTypeVariables, DataKinds, TypeFamilies #-}
module Haskgine.Renderer.DeferredRenderer where

import Data.Word

import Haskgine.Renderer.Renderer
import Haskgine.Renderer.ShadowRenderer as SR
import Haskgine.Graphics.Camera
import Haskgine.Graphics.Shaders
import Haskgine.Graphics.GBuffer

import Haskgine.Graphics.Entity (Entity(..))
import qualified Haskgine.Graphics.Entity as Entity

import Haskgine.Util.GL.Shader
import Haskgine.Util.GL.Program
import Haskgine.Util.Linalg

import qualified Haskgine.Graphics.Shaders.EntityShader as ES
import qualified Haskgine.Graphics.Shaders.FinalizeShader as FP

import qualified Graphics.Rendering.OpenGL as GL

import Numeric.LinearAlgebra.Static

import GHC.IO.Exception

import Control.Monad.Except
import Control.Monad.IO.Class
import Data.StateVar
import Data.Default
import Util.Errors

data DeferredRenderer = DeferredRenderer
  { _shadowRenderer :: ShadowRenderer
  , _gBuffer :: GBuffer
  , _entityProgram :: EntityProgram
  , _finalizeProgram :: FinalizeProgram
  , _renderWidth :: Word32
  , _renderHeight :: Word32
  , _shadowRenderTarget :: ShadowRenderTarget
  }

-- | Purely utility data for ease of use
data DeferredRendererShaderFiles = DeferredRendererShaderFiles
  { _shadowVertFp :: FilePath
  , _shadowFragFp :: FilePath

  , _entityVertFp :: FilePath
  , _entityFragFp :: FilePath

  , _finalizeVertFp :: FilePath
  , _finalizeFragFp :: FilePath

  } deriving (Show, Read, Eq)

data DeferredRenderPrograms = DeferredRenderPrograms
  { _shadowProgram' :: ShadowProgram
  , _entityProgram' :: EntityProgram
  , _finalizeProgram' :: FinalizeProgram
  }

makeDeferredPrograms :: (MonadError String m, MonadIO m) => DeferredRendererShaderFiles -> m DeferredRenderPrograms
makeDeferredPrograms DeferredRendererShaderFiles{..} = do
  shadowVert <- liftShowError $ fromSourceFile _shadowVertFp
  shadowFrag <- liftShowError $ fromSourceFile _shadowFragFp
  shadowProgram <- liftError show $ runExceptT $ makeShadowProgram shadowVert shadowFrag

  deferredVert <- liftShowError $ fromSourceFile _entityVertFp
  deferredFrag <- liftShowError $ fromSourceFile _entityFragFp
  deferredProgram <- liftError show $ runExceptT $ makeEntityProgram deferredVert deferredFrag

  finalizeVert <- liftShowError $ fromSourceFile _finalizeVertFp
  finalizeFrag <- liftShowError $ fromSourceFile _finalizeFragFp
  finalizeProgram <- liftError show $ (runExceptT (makeFinalizeProgram finalizeVert finalizeFrag))
  pure $ DeferredRenderPrograms
    { _shadowProgram' = shadowProgram
    , _entityProgram' = deferredProgram
    , _finalizeProgram' = finalizeProgram
    }
  where
    liftShowError = liftError show . runExceptT


makeDeferredRenderer :: (MonadError String m, MonadIO m) => DeferredRenderPrograms -> Word32 -> Word32 -> Word32 -> Word32 -> m DeferredRenderer
makeDeferredRenderer DeferredRenderPrograms{..} renderWidth renderHeight shadowWidth shadowHeight = do
  shadowRenderer <- makeShadowRenderer _shadowProgram'
  shadowRenderTarget <- makeShadowRenderTarget shadowRenderer shadowWidth shadowHeight
  gBuffer <- makeGBuffer renderWidth renderHeight
  pure $ DeferredRenderer
    { _entityProgram = _entityProgram'
    , _finalizeProgram = _finalizeProgram'
    , _renderWidth = renderWidth
    , _renderHeight = renderHeight
    , _gBuffer = gBuffer
    , _shadowRenderer = shadowRenderer
    , _shadowRenderTarget = shadowRenderTarget
    }
  where
    toErr action = do
      a <- action
      case a of
        Left err -> throwError (ProgramShaderError err)
        Right res -> pure res

renderDeffered :: (MonadIO m, Traversable t) => DeferredRenderer -> Camera -> Maybe ShadowRenderSettings -> SimpleRenderTarget -> t Entity -> m ()
renderDeffered DeferredRenderer{..} camera mShadowRenderSettings renderTarget entities = do
    -- Set the viewport to be correct
    GL.viewport $=! (GL.Position 0 0,
                     GL.Size (fromIntegral _renderWidth) (fromIntegral _renderHeight))
--    liftIO $ GL.clear [GL.ColorBuffer, GL.DepthBuffer]

    -- Bind and sets the correct checks for the gbuffer rendering
    bindGBuffer _gBuffer
    liftIO $ GL.clear [GL.ColorBuffer, GL.DepthBuffer]

    GL.frontFace $=! GL.CCW
    GL.stencilTest $=! GL.Disabled
    GL.blend $=! GL.Disabled
    GL.alphaFunc $=! Nothing
    GL.depthFunc $=! Just GL.Less
    GL.cullFace $=! Just GL.Back

    bindProgram _entityProgram

    withGLProgram _entityProgram $ do
      ES.useProjection _entityProgram $ projection camera -- translationMatrix (vec3 0 0 0) 
      ES.useView _entityProgram (cameraLookAtMat camera)
      forM_ entities $ \entity -> do
        Entity.draw _entityProgram entity

    GL.cullFace $=! Nothing
    GL.depthFunc $=! Nothing

    case mShadowRenderSettings of
      Just shadowRenderSettings ->
        render _shadowRenderer shadowRenderSettings () _shadowRenderTarget entities
      _ -> pure ()

    -- Finalization
    GL.bindFramebuffer GL.Framebuffer $=! _framebuffer renderTarget
    GL.viewport $=! (_viewportPosition renderTarget, _viewportSize renderTarget)
    liftIO $ GL.clear [GL.ColorBuffer, GL.DepthBuffer]


    let proj = maybe eye getShadowProjection mShadowRenderSettings

    withGLProgram _finalizeProgram $ do
      FP.useCameraPosition  _finalizeProgram (_camPosition camera)
      FP.useLightProjection _finalizeProgram proj
      FP.useGlobalLightPosition _finalizeProgram (vec3 10 40 0)
      FP.bindGBufferTextures _finalizeProgram _gBuffer
      FP.bindShadowTexture _finalizeProgram (_shadowTexture _shadowRenderTarget)
      FP.draw _finalizeProgram
    pure ()
      -- FP.bindShadowMap


instance Renderer DeferredRenderer where
  type RenderPerspective DeferredRenderer = Camera
  type RenderSettings DeferredRenderer = Maybe ShadowRenderSettings
  type RenderTarget DeferredRenderer = SimpleRenderTarget
  render renderer perspective settings target entities =
    renderDeffered renderer perspective settings target entities

-- | Author: Tomas Möre 2019
{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, TypeFamilies#-}

module Haskgine.Renderer.Renderer
  ( RenderTarget(..)
  , GL.Position(..)
  , GL.Size(..)
  , Renderer(..)
  , SimpleRenderTarget(..)
  ) where

import Data.Word

import Haskgine.Graphics.Entity
import Haskgine.Graphics.Camera

import qualified Graphics.Rendering.OpenGL as GL

import Control.Monad.IO.Class

import Data.Default

data SimpleRenderTarget = MkSimpleRenderTarget
                    { _framebuffer      :: GL.FramebufferObject
                    , _viewportPosition :: GL.Position
                    , _viewportSize     :: GL.Size
                    }

instance Default SimpleRenderTarget where
  def = MkSimpleRenderTarget
    { _framebuffer      = GL.defaultFramebufferObject
    , _viewportPosition = GL.Position 0 0
    , _viewportSize     = GL.Size 0 0
    }

class Renderer r where
  type RenderPerspective r :: *
  type RenderSettings r :: *
  type RenderTarget r :: *
  render :: (MonadIO m, Traversable t) => r -> RenderPerspective r -> RenderSettings r -> RenderTarget r -> t Entity -> m ()

-- | Author: Tomas Möre 2019
{-# LANGUAGE RecordWildCards, MultiParamTypeClasses, FunctionalDependencies, FlexibleContexts, RecordWildCards, DataKinds, TypeFamilies #-}
module Haskgine.Renderer.ShadowRenderer where

import qualified Graphics.Rendering.OpenGL as GL
import Data.StateVar

import Data.Word
import Foreign.Ptr (nullPtr)

import Numeric.LinearAlgebra.Static (mul, (#>), vec4, extract, tr, L(..), eye)

import Control.Monad
import Control.Monad.Except
import Control.Monad.IO.Class

import Data.Default

import Haskgine.Util.GL.Texture
import Haskgine.Util.GL.Program
import Haskgine.Util.Linalg
import Haskgine.Graphics.Shaders
import Haskgine.Graphics.Shaders.ShadowShader as SS
import Haskgine.Renderer.Renderer
import Haskgine.Graphics.Entity (Entity(..))
import qualified  Haskgine.Graphics.Entity as Entity
import qualified Haskgine.Util.GL.VAO as VAO

data ShadowRenderer = ShadowRenderer
                      { _shadowProgram :: ShadowProgram
                      }

data ShadowRenderTarget = ShadowRenderTarget
  { _shadowWidth    :: Word32
  , _shadowHeight  :: Word32
  , _shadowTexture :: Texture2D
  , _shadowFramebuffer :: GL.FramebufferObject
  }


data ShadowRenderSettings = ShadowRenderSettings
  { _shadowProjection :: L 4 4
  , _shadowPosition :: R 3
  , _shadowLookAt   :: R 3
  }

getShadowProjection :: ShadowRenderSettings -> L 4 4
getShadowProjection ShadowRenderSettings{..} =
  let pos = lookAtMatrixRH  _shadowPosition _shadowLookAt (vec3 0 0 (-1))
  in  mul pos _shadowProjection


instance Default ShadowRenderSettings where
  -- | This default is entirely arbitary, however pure zeros would yeild an invalid result in most cases
  def = ShadowRenderSettings
    { _shadowProjection = orthographicProjection 50 50 0.1 100
    , _shadowPosition = vec3 0 200 0
    , _shadowLookAt = vec3 0 0 (-1)
    }

makeShadowRenderer :: (MonadError String m, MonadIO m) => ShadowProgram -> m ShadowRenderer
makeShadowRenderer shadowProgram = do
  return ShadowRenderer
    { _shadowProgram = shadowProgram }

makeShadowRenderTarget  :: (MonadError String m, MonadIO m) => ShadowRenderer -> Word32 -> Word32 -> m ShadowRenderTarget
makeShadowRenderTarget program width height = do
  (shadowTexture, framebuffer) <- makeShadowFramebuffer width height
  pure $ ShadowRenderTarget
    { _shadowWidth   = width
    , _shadowHeight  = height
    , _shadowTexture = shadowTexture
    , _shadowFramebuffer = framebuffer
    }

makeShadowFramebuffer :: (MonadError String m, MonadIO m) => Word32 -> Word32 -> m (Texture2D, GL.FramebufferObject)
makeShadowFramebuffer width height = do
  framebuffer <- GL.genObjectName
-- glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
  shadowTexture <- newTextureWithData GL.Texture2D
                                      (texSize2D width height)
                                      GL.DepthComponent'
                                      (GL.Linear', Nothing)
                                      GL.Linear'
                                      (Just (PixelData GL.DepthComponent GL.Float nullPtr))
  bindTexture shadowTexture

  GL.bindFramebuffer GL.Framebuffer $=! framebuffer
  framebufferTexture shadowTexture GL.Framebuffer GL.DepthAttachment GL.Texture2D 0
  GL.drawBuffer $=! GL.NoBuffers
  GL.readBuffer $=! GL.NoBuffers
  shadowFBStatus <- get (GL.framebufferStatus GL.Framebuffer)
  when (shadowFBStatus /= GL.Complete) $ throwError (show shadowFBStatus)

  -- Dunno if this matters
  -- GL.bindFramebuffer GL.ReadFramebuffer $=! GL.defaultFramebufferObject

  fbStatus <- get (GL.framebufferStatus GL.Framebuffer)
  GL.bindFramebuffer GL.Framebuffer $=! GL.defaultFramebufferObject
  when (fbStatus /= GL.Complete) $ throwError (show fbStatus)
  pure (shadowTexture, framebuffer)


renderShadow :: (MonadIO m, Traversable t) => ShadowRenderer -> ShadowRenderSettings -> ShadowRenderTarget -> t Entity -> m ()
renderShadow ShadowRenderer{..} settings ShadowRenderTarget{..}  entities = liftIO $  do
    GL.viewport $=! (GL.Position 0 0,
                     GL.Size (fromIntegral _shadowWidth) (fromIntegral _shadowHeight))
    GL.bindFramebuffer GL.Framebuffer $=! _shadowFramebuffer
    GL.depthFunc $=! Just GL.Less
    GL.cullFace $=! (Just GL.Back)
    GL.clear [GL.DepthBuffer]

    let proj = getShadowProjection settings

    withGLProgram _shadowProgram $ do
      SS.bindLightProjection _shadowProgram proj
      forM_ entities $ \entity@Entity{..} -> do
        SS.bindModelTransformation _shadowProgram (transformMatrix entity)
        VAO.draw _vao
    GL.cullFace $=! Nothing
    GL.depthFunc $=! Nothing
    GL.bindFramebuffer GL.Framebuffer $=! GL.defaultFramebufferObject

instance Renderer ShadowRenderer where
  type RenderPerspective ShadowRenderer = ShadowRenderSettings
  type RenderSettings ShadowRenderer = ()
  type RenderTarget ShadowRenderer = ShadowRenderTarget
  render renderer perspective () target entities =
    renderShadow renderer perspective target entities

-- instance HasRenderTarget ShadowRenderer Texture2D where
--   renderTargetSize ShadowRenderer{..} = (_shadowWidth, _shadowHeight)
--   updateRenderTargetSize render width height = undefined
--   renderTarget ShadowRenderer{..} = _shadowTexture

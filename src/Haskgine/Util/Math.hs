module Haskgine.Util.Math where

-- | clamps the value between low and high (inclusive)
clamp :: Ord a => a -> a -> a -> a
clamp low high x
  | x <= low = low
  | x >= high = high
  | otherwise = x

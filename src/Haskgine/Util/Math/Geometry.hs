{-# LANGUAGE DataKinds, RecordWildCards, MultiWayIf, MultiParamTypeClasses #-}
module Haskgine.Util.Math.Geometry where


import Haskgine.Util.Linalg

data RealRange = 
  MkRealRange
  { _center :: Double
  , _radius :: Double
  } deriving ( Show )
  

realRange :: Double -> Double -> RealRange
realRange a b =
  MkRealRange (min_p + radius) radius
  where
    min_p = min a b
    max_p = max a b
    diameter = max_p - min_p
    radius = 0.5 * diameter

realRangeIntersects :: RealRange -> RealRange -> Bool
realRangeIntersects (MkRealRange c1 r1) (MkRealRange c2 r2) =
  if r1 > r2
  then c1 - r1 <= c2 && c2 <= c1 + r1
  else c2 - r2 <= c1 && c1 <= c2 + r2


-- | Seen as unit cube at position scaled by scale
data AABB =
  MkAABB
  { _scale :: R 3
  , _position :: R 3
  } deriving (Show )

cuboidContainsPoint :: AABB -> R 3 -> Bool
cuboidContainsPoint MkAABB{..} point =
  isWithin (dimX _scale) (dimX _position) (dimX point)
  && isWithin (dimY _scale) (dimY _position) (dimY point)
  &&  isWithin (dimZ _scale) (dimZ _position) (dimZ point)
  where
    isWithin dist center x =
      inRange (center - dist) (center + dist) x
      
    inRange min max val =
      min <= val && val <= max

cuboidIntersects :: AABB -> AABB -> Bool
cuboidIntersects (MkAABB rs1 ps1) (MkAABB rs2 ps2) = 
  realRangeIntersects (MkRealRange (dimX ps1) (dimX rs1)) (MkRealRange (dimX ps2) (dimX rs2))
  && realRangeIntersects (MkRealRange (dimY ps1) (dimY rs1)) (MkRealRange (dimY ps2) (dimY rs2))
  && realRangeIntersects (MkRealRange (dimZ ps1) (dimZ rs1)) (MkRealRange (dimZ ps2) (dimZ rs2))  


cuboidVertices :: AABB -> [ R 3 ]
cuboidVertices MkAABB{..} =
  [ _position + _scale
  , _position + vec3 (-x) y z
  , _position + vec3 x (-y) z
  , _position + vec3 x y (-z)
  , _position + vec3 (-x) (-y) z
  , _position + vec3 (-x) y (-z)
  , _position + vec3 x (-y) (-z)
  , _position + vec3 (-x) (-y) (-z)
  ]
  where
    x = dimX _scale
    y = dimY _scale
    z = dimZ _scale


class InterSectable a b where
  intersects :: a -> b -> Bool


instance InterSectable AABB AABB where
  intersects = cuboidIntersects
    -- any (cuboidContainsPoint cuboid1) (cuboidVertices cuboid2)


class Geometry3D a where
  centerOf3D ::  a -> R 3

instance Geometry3D AABB where
  centerOf3D MkAABB{..} = _position

class Is3DSpace a where
  contains3DPoint :: a -> R 3 -> Bool

instance Is3DSpace AABB where
  contains3DPoint = cuboidContainsPoint



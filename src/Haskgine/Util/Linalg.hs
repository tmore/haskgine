{-# LANGUAGE DataKinds #-}
module Haskgine.Util.Linalg
  ( module Haskgine.Util.Linalg.Transform
  , module Haskgine.Util.Linalg.Projection
  , module Haskgine.Util.Linalg.Matrices
  , module Haskgine.Util.Linalg.Vector  
  , R
  , rotateVec3
  , vec3
  , cross
  , dot
  )
where


import Haskgine.Util.Linalg.Transform
import Haskgine.Util.Linalg.Projection
import Haskgine.Util.Linalg.Matrices
import Haskgine.Util.Linalg.Vector  

import Numeric.LinearAlgebra.Static

-- | Rotates a vector around a pivet element, assumes pivot is normalized.
-- This could probabiy be made more efficent by removing the intermediate matrix
rotateVec3 :: R 3 -> Double -> R 3 -> R 3
rotateVec3 pivot rads vec =
  rotateMatrix3 pivot rads #> vec


{-# LANGUAGE TypeFamilies, ScopedTypeVariables, RecordWildCards, TupleSections, RecursiveDo #-}
module Haskgine.Util.Resources.Manager
  ( ResourceManager
  , empty
  , nameSet
  , names
  , toList
  , insertM
  , delete
  , lookup
  , update
  , updateM
  ) where

import Prelude hiding (lookup)

import Control.Monad.Except

import Data.Word

import Data.Set (Set)
import qualified Data.Set as Set
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as Map
import Data.Hashable

import Data.Traversable

import Haskgine.Util.Resources.Util (ResourceKey(..), reKey)

import Haskgine.Util.Resources.Store (ResourceStore)
import qualified Haskgine.Util.Resources.Store as Store

-- | Resource names are names used for identifying resources before it is
-- inserted into the manager. It should not be used except for when attempting
-- to insert new resouces
data ResourceManager n r = ResourceManager
  { _resourceStore :: ResourceStore (r, n)
  , _resourceNames :: HashMap n (ResourceKey r)
  }

empty :: ResourceManager n r
empty = ResourceManager Store.empty Map.empty

nameSet :: Ord n => ResourceManager n r -> Set n
nameSet = Set.fromList . Map.keys .  _resourceNames

names :: ResourceManager n r -> [n]
names = Map.keys . _resourceNames

toList :: ResourceManager n r -> [r]
toList = fmap fst . Store.toList . _resourceStore

insertM :: (Ord n, Monad m, Hashable n) => ResourceManager n r -> n -> (ResourceKey r -> m r) -> m (ResourceKey r, ResourceManager n r)
insertM manager@ResourceManager{..} name initF =
  case Map.lookup name _resourceNames of
    Just id -> pure (id, manager)
    Nothing -> do
      (storeKey, store') <- Store.insertWithKeyM _resourceStore
                                                 (\ key-> (,name) <$> initF (reKey key))
      let key = reKey storeKey
      pure (key
           , manager{ _resourceStore =
                        store'
                    , _resourceNames =
                        Map.insert name key _resourceNames
                    })

delete :: (Eq n, Hashable n) => ResourceManager n r -> ResourceKey r -> ResourceManager n r
delete manager@ResourceManager{..} key =
  case Store.lookup (reKey key) _resourceStore of
    Nothing -> manager
    Just (resource, name) ->
      manager{ _resourceStore =
                 Store.delete (reKey key) _resourceStore
             , _resourceNames =
                 Map.delete name _resourceNames
             }

lookup :: ResourceKey r -> ResourceManager n r -> Maybe r
lookup key ResourceManager{..} =
  fst <$> Store.lookup (reKey key) _resourceStore

update :: (Eq n, Hashable n) => ResourceManager n r -> ResourceKey r -> (r -> Maybe r) -> ResourceManager n r
update manager@ResourceManager{..} key@(ResourceKey i) updF =
  manager{ _resourceStore = Store.update _resourceStore (reKey key) (\ (r,n) -> (,n) <$> updF r) }

updateM :: (Ord n, Hashable n, Monad m) => ResourceManager n r -> ResourceKey r -> (r -> m (Maybe r)) -> m (ResourceManager n r)
updateM manager@ResourceManager{..} key updF =
  case mOld of
    Nothing -> pure manager
    Just (old, name) -> do
      mNew <- updF old
      pure manager{ _resourceStore =
                      Store.update _resourceStore (reKey key) (\(r,n) ->
                                                                 (,n) <$> mNew
                                                              )
                  , _resourceNames = case mNew of
                                      Nothing -> Map.delete name _resourceNames
                                      _ -> _resourceNames
                  }
  where
    mOld = Store.lookup (reKey key) _resourceStore

-- mapMaybe :: ResourceManager n r -> (r -> Maybe r) -> ResourceManager n r
-- updateAll m updF =
--   Map.foldlWithKey (\ m' key (r,n) ->
--                       case updF n of
--                         Nothing ->
--                           delete m' key
--                         Just r' ->
--                           m'{ _resourseStore =
--                                 Map.adjust (const (r', n)) (_resourceStore m')
--                             }
--                       ) m

instance Functor (ResourceManager n) where
--  fmap :: (a -> b) -> ResourceManager n a -> ResourceManager n b
  fmap f manager@ResourceManager{..} =
    ResourceManager
    { _resourceStore = fmap (\ (r,name) -> (f r, name)) _resourceStore
    , _resourceNames = fmap (\ (ResourceKey n) -> ResourceKey n) _resourceNames
    }

-- instance Traversable (ResourceManager n) where
--   traverse f manager@ResourceManager{..} =
--     manager{ _resourceStore =
--                traverse (\ (r,n) -> (,) <$> f r)
--            }

{-# LANGUAGE RecordWildCards #-}
module Haskgine.Util.Resources.Store where

import Data.HashMap.Strict (HashMap)
import Data.HashMap.Strict as HashMap
import Data.Word
import Haskgine.Util.Resources.Util (ResourceKey(..))

-- | A resource store is an object where resources are stored to be changed
data ResourceStore a = ResourceStore
                       { _store :: HashMap Word64 a
                       , _nextResourceKey :: Word64
                       }

empty :: ResourceStore r
empty = ResourceStore mempty 0

toList :: ResourceStore r -> [r]
toList = HashMap.elems . _store

lookup :: ResourceKey r -> ResourceStore r ->  Maybe r
lookup (ResourceKey key) ResourceStore{..} =
  HashMap.lookup key _store

insert :: ResourceStore r -> r -> (ResourceKey r, ResourceStore r)
insert store@ResourceStore{..} resource =
  (ResourceKey _nextResourceKey
  , store{ _store =
               HashMap.insert _nextResourceKey resource _store
           , _nextResourceKey =
               _nextResourceKey + 1
           })

insertWithKeyM :: Monad m => ResourceStore r -> (ResourceKey r -> m r) -> m (ResourceKey r, ResourceStore r)
insertWithKeyM store@ResourceStore{..} resourceMaker = do
  let key = ResourceKey  _nextResourceKey
  resource <- resourceMaker key
  pure (key
       , store{ _store =
                    HashMap.insert _nextResourceKey resource _store
                , _nextResourceKey =
                    _nextResourceKey + 1
                })

delete :: ResourceKey r -> ResourceStore r -> ResourceStore r
delete (ResourceKey key) store@ResourceStore{..}  =
  case HashMap.lookup key _store of
    Nothing ->
      store
    Just _ ->
      store{ _store = HashMap.delete key _store }

update :: ResourceStore r -> ResourceKey r -> (r -> Maybe r) -> ResourceStore r
update store@ResourceStore{..} (ResourceKey i) updF =
  store{ _store = HashMap.update (\ r -> updF r) i _store }

updateM :: (Monad m) => ResourceStore r -> ResourceKey r -> (r -> m (Maybe r)) -> m (ResourceStore r)
updateM store@ResourceStore{..} (ResourceKey i) updF =
  case mOld of
    Nothing -> pure store
    Just old -> do
      mNew <- updF old
      pure store{
        _store =
            HashMap.update (\ r ->
                           mNew
                       ) i _store
        }
  where
    mOld = HashMap.lookup i _store

mapMaybe :: ResourceStore r -> (r -> Maybe r) -> ResourceStore r
mapMaybe store updF =
  store{
  _store = HashMap.mapMaybe updF (_store store)
  }

instance Functor ResourceStore where
--  fmap :: (a -> b) -> ResourceManager n a -> ResourceManager n b
  fmap f ResourceStore{..} =
    ResourceStore
    { _store = fmap (\ r -> f r) _store
    , _nextResourceKey = _nextResourceKey
    }

instance Foldable ResourceStore where
  foldr f b ResourceStore{..} = Prelude.foldr f b _store

instance Traversable ResourceStore where
  traverse f store@ResourceStore{..} =
    let inner = traverse (\ r -> f r) _store
    in (\new -> store {_store = new}) <$> inner

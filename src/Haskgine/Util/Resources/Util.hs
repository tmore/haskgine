module Haskgine.Util.Resources.Util where

import Data.Word
import Data.Hashable


newtype ResourceKey r = ResourceKey Word64
                     deriving (Show, Read, Eq, Ord)

instance Hashable (ResourceKey r) where
  hashWithSalt salt (ResourceKey k) = hashWithSalt salt k

reKey :: ResourceKey a -> ResourceKey b
reKey (ResourceKey k) = ResourceKey k

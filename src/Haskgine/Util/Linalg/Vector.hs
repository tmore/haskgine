{-# LANGUAGE DataKinds #-}

module Haskgine.Util.Linalg.Vector where

import Data.Coerce 
import GHC.TypeLits
import Numeric.LinearAlgebra.Static
import Numeric.LinearAlgebra.Data ((!))

dimX :: R 3 -> ℝ
dimX v = unwrap v ! 0

dimY :: R 3 -> ℝ
dimY v = unwrap v ! 1

dimZ :: R 3 -> ℝ
dimZ v = unwrap v ! 2

normalize ::KnownNat n => R n -> R n
normalize v = dvmap (*n) v
  where
    n = 1 / norm_2 v

vec3FromDouble :: Double -> R 3
vec3FromDouble z = vec3 z z z

vecScalarMultiply ::  KnownNat n => Double -> R n -> R n
vecScalarMultiply z = dvmap (*z)

updateX :: (Double -> Double) -> R 3 -> R 3
updateX f v3 = vec3 (f (dimX v3)) (dimY v3) (dimZ v3)

updateY :: (Double -> Double) -> R 3 -> R 3
updateY f v3 = vec3 (dimX v3) (f (dimY v3)) (dimZ v3)

updateZ :: (Double -> Double) -> R 3 -> R 3
updateZ f v3 = vec3 (dimX v3) (dimY v3) (f (dimZ v3))

multiplyX :: Double -> R 3 -> R 3
multiplyX z vec = vec3 (z * dimX vec) (dimY vec) (dimZ vec)

multiplyY :: Double -> R 3 -> R 3
multiplyY z vec = vec3 (dimX vec) (z * dimY vec) (dimZ vec)

multiplyZ :: Double -> R 3 -> R 3
multiplyZ z vec = vec3 (dimX vec) (dimY vec) (z * dimZ vec)




{-# LANGUAGE DataKinds #-}

module Haskgine.Util.Linalg.Matrices where

import Haskgine.Util.Linalg.Vector

import GHC.TypeLits
import Numeric.LinearAlgebra.Static as S


import Numeric.LinearAlgebra.Data ((!))

scalarMultiply :: (KnownNat m, KnownNat n) => ℝ -> L n m -> L n m
scalarMultiply scalar matrix = dmmap (*scalar) matrix


translationMatrix :: R 3 -> L 4 4
translationMatrix v = matrix
  [ 1, 0, 0, dimX v
  , 0, 1, 0, dimY v
  , 0, 0, 1, dimZ v
  , 0, 0, 0,      1
  ]


scaleMat3 :: Double -> Double -> Double -> L 3 3
scaleMat3 x y z = matrix
  [ x, 0, 0
  , 0, y, 0
  , 0, 0, z
  ]

scaleMat4 :: Double -> Double -> Double -> L 4 4
scaleMat4 x y z = matrix
  [ x, 0, 0, 0
  , 0, y, 0, 0
  , 0, 0, z, 0
  , 0, 0, 0, 1
  ]




-- lookAtMatrixLH :: R 3 -> R 3 ->  R 3 -> L 4 4
-- lookAtMatrixLH position target up =
--   matrix [ dimX xaxis, dimX yaxis, dimX zaxis, 0
--          , dimY xaxis, dimY yaxis, dimY zaxis, 0
--          , dimZ xaxis, dimZ yaxis, dimZ zaxis, 0
--          , negate $ (dot xaxis position),  negate $ (dot yaxis position), negate $ (dot zaxis position),  1
--          ]
--   where
--     zaxis = normalize $ target - position
--     xaxis = normalize $ cross up zaxis
--     yaxis = cross zaxis xaxis

-- lookAtMatrixLH :: R 3 -> R 3 ->  R 3 -> L 4 4
-- lookAtMatrixLH position target up =
--   matrix [ dimX xaxis, dimX yaxis, dimX zaxis, 0
--          , dimY xaxis, dimY yaxis, dimY zaxis, 0
--          , dimZ xaxis, dimZ yaxis, dimZ zaxis, 0
--          , negate $ (dot xaxis position),  negate $ (dot yaxis position), negate $ (dot zaxis position),  1
--          ]
--   where
--     zaxis = normalize $ target - position
--     xaxis = normalize $ cross up zaxis
--     yaxis = cross zaxis xaxis

lookAtMatrixLH :: R 3 -> R 3 ->  R 3 -> L 4 4
lookAtMatrixLH eye at up =  matrix
  [ dimX xaxis, dimY xaxis, dimZ xaxis, negate (dot xaxis eye)
  , dimX yaxis, dimY yaxis, dimZ yaxis, negate (dot yaxis eye)
  , dimX zaxis, dimY zaxis, dimZ zaxis, negate (dot zaxis eye)
  , 0,                   0,          0,                      1
  ]
  where
    zaxis = normalize $ at - eye
    xaxis = normalize $ cross zaxis up
    yaxis = cross xaxis zaxis



-- lookAtMatrixRH :: R 3 -> R 3 ->  R 3 -> L 4 4
-- lookAtMatrixRH position target up =
--   matrix [ dimX xaxis, dimX yaxis, dimX zaxis, 0
--          , dimY xaxis, dimY yaxis, dimY zaxis, 0
--          , dimZ xaxis, dimZ yaxis, dimZ zaxis, 0
--          , negate $ (dot xaxis position),  negate $ (dot yaxis position), negate $ (dot zaxis position),  1
--          ]
--   where
--     zaxis = normalize $ (position - target)
--     xaxis = normalize $ cross up zaxis
--     yaxis = cross zaxis xaxis

lookAtMatrixRH :: R 3 -> R 3 ->  R 3 -> L 4 4
lookAtMatrixRH position target up =
  -- matrix [ dimX xaxis, dimX yaxis, dimX zaxis, negate (dot xaxis position)
  --        , dimY xaxis, dimY yaxis, dimY zaxis, negate (dot yaxis position)
  --        , dimZ xaxis, dimZ yaxis, dimZ zaxis, negate (dot zaxis position)
  --        ,          0,          0,          0, 1
  --        ]

  matrix [ dimX xaxis, dimY xaxis, dimZ xaxis, -(dot xaxis position)
         , dimX yaxis, dimY yaxis, dimZ yaxis, -(dot yaxis position)
         , dimX zaxis, dimY zaxis, dimZ zaxis, -(dot zaxis position)
         ,          0,          0,          0,                    1
         ]
  where
    zaxis = normalize (position - target)
    xaxis = normalize $ cross up zaxis
    yaxis = cross zaxis xaxis


{-|

>>> let position = vec3 0 0 0
>>> let target = vec3 0 1 0
>>> let up = vec3 (-1) 0 0
>>> let zaxis = normalize (position - target)
>>> zaxis
>>> cross up zaxis
>>> let xaxis = normalize $ cross up zaxis
>>> xaxis
(vector [0.0,-1.0,0.0] :: R 3)
WAS (vector [0.0,0.0,-0.0] :: R 3)
WAS (vector [NaN,NaN,NaN] :: R 3)
NOW (vector [0.0,0.0,1.0] :: R 3)
NOW (vector [0.0,0.0,1.0] :: R 3)


>>> let yaxis = cross zaxis xaxis
>>> yaxis
WAS (vector [NaN,NaN,NaN] :: R 3)
NOW (vector [-1.0,0.0,0.0] :: R 3)



  matrix [ dimX xaxis, dimY xaxis, dimZ xaxis, -(dot xaxis position), dimX yaxis, dimY yaxis, dimZ yaxis, -(dot yaxis position), dimX zaxis, dimY zaxis, dimZ zaxis, -(dot zaxis position)           0,          0,          0,                    1
         ]
>>>   (matrix [ dimX xaxis, dimY xaxis, dimZ xaxis, -(dot xaxis position), dimX yaxis, dimY yaxis, dimZ yaxis, -(dot yaxis position), dimX zaxis, dimY zaxis, dimZ zaxis, -(dot zaxis position),          0,          0,          0,                    1 ] :: L 4 4)
WAS WAS No instance for (KnownNat n0) arising from a use of ‘matrix’
WAS NOW (matrix
WAS  [ NaN,  NaN, NaN,  NaN
WAS  , NaN,  NaN, NaN,  NaN
WAS  , 0.0, -1.0, 0.0, -0.0
WAS  , 0.0,  0.0, 0.0,  1.0 ] :: L 4 4)
NOW (matrix
 [  0.0,  0.0, 1.0, -0.0
 , -1.0,  0.0, 0.0, -0.0
 ,  0.0, -1.0, 0.0, -0.0
 ,  0.0,  0.0, 0.0,  1.0 ] :: L 4 4)





-}

{-|
>>> lookAtMatrixRH (vec3 0 0 0) (vec3 0 1 0) (vec3 0 1 0)
>>> lookAtMatrixRH (vec3 0 0 0) (vec3 0 1 0) (vec3 0 1 0) #> vec4 0 1 0 1









-}

perspectiveRH :: ℝ -> ℝ -> ℝ -> ℝ -> L 4 4
perspectiveRH fov aspect near far =
  matrix [ w, 0, 0, 0
         , 0, h, 0, 0
         , 0, 0, - ((far + near) / (far - near)),  - ( (2 * far * near) / (far - near) )
         , 0, 0, -1, 0
         ]
  where
    h = 1 / tan ( 0.5 * fov )
    w = 1 / (aspect * tan ( 0.5 * fov ))

-- perspectiveLH :: ℝ -> ℝ -> ℝ -> ℝ -> L 4 4
-- perspectiveLH fov aspect near far =
--   matrix [ w, 0, 0, 0
--          , 0, h, 0, 0
--          , 0, 0, far /(far - near),  1
--          , 0, 0, near * far / (near - far), 0
--          ]
--   where
--     frustumDepth = far - near
--     oneOverDepth = 1 / frustumDepth
--     h = 1 / tan (0.5 * fov)
--     w = h / aspect

rotateZMat3 :: Double -> L 3 3
rotateZMat3 angle = matrix
  [ cos angle, -sin angle, 0
  , sin angle,  cos angle,  0
  , 0,          0,          1
  ]

rotateZMat4 :: Double -> L 4 4
rotateZMat4 angle = matrix
  [ cos angle, -sin angle, 0,  0
  , sin angle,  cos angle,  0, 0
  , 0,          0,          1, 0
  , 0,          0,          0, 1
  ]

rotateXMat3 :: Double -> L 3 3
rotateXMat3 a = matrix
  [ 1,      0,        0
  , 0,  cos a,   -sin a
  , 0,  sin a,    cos a
  ]


rotateXMat4 :: Double -> L 4 4
rotateXMat4 a = matrix
  [ 1,      0,       0,  0
  , 0,  cos a,  -sin a,  0
  , 0,  sin a,   cos a,  0
  , 0,      0,       0,  1
  ]

rotateYMat3 :: Double -> L 3 3
rotateYMat3 a = matrix
  [ cos a,   0,   sin a
  , 0,       1,   0
  , -sin a,  0,   cos a
  ]

rotateYMat4 :: Double -> L 4 4
rotateYMat4 a = matrix
  [  cos a,  0,   sin a, 0
  ,      0,  1,       0, 0
  , -sin a,  0,   cos a, 0
  ,      0,  0,       0, 1
  ]

rotateAroundM3 :: R 3 -> ℝ -> L 3 3
rotateAroundM3 v angle =
  eye + scalarMultiply (sin angle) w + scalarMultiply (2 * (sin (angle /2)) ** 2) (w `mul` w)
  where
    w = matrix [ 0,               negate (dimZ v), dimY v
               , dimZ v,          0,               negate (dimX v)
               , negate (dimY v), dimX v,          0
               ]

rotateAroundM4 :: R 3 -> ℝ -> L 4 4
rotateAroundM4 v angle =
  eye + scalarMultiply (sin angle) w + scalarMultiply (2 * (sin (angle /2)) ** 2) (w `mul` w)
  where
    w = matrix [ 0,               negate (dimZ v), dimY v,          0
               , dimZ v,          0,               negate (dimX v), 0
               , negate (dimY v), dimX v,          0,               0
               , 0,               0,               0,               1
               ]

-- rotateMatrix :: R 3 -> ℝ -> L 3 3
-- rotateMatrix v angle =
--   eye + scalarMultiply (sin angle) w + scalarMultiply (2 * (sin (angle /2)) ** 2) (w `mul` w)
--   where
--     w = matrix [ 0,               negate (dimZ v), dimY v
--                , dimZ v,          0,               negate (dimX v)
--                , negate (dimY v), dimX v,          0
--                ]

rotateMatrix3 :: R 3 -> ℝ -> L 3 3
rotateMatrix3 v angle =
  scalarMultiply (cos angle) eye + scalarMultiply (sin angle) w +  scalarMultiply (1 - cos angle) (col v `mul` row v)
  where
    w = matrix [ 0,               negate (dimZ v), dimY v
               , dimZ v,          0,               negate (dimX v)
               , negate (dimY v), dimX v,          0
               ]
-- RH
orthographicProjection :: ℝ -> ℝ -> ℝ -> ℝ -> L 4 4
orthographicProjection width height near far =
  matrix [ 2/w,   0,       0, 0
         ,   0, 2/h,       0, 0
         ,   0,   0, 1/(n-f), 0
         ,   0,   0, n/(n-f), 1]
  where
    f = far
    n = near
    w = width
    h = height


{-|
>>> scaleMat4 2 2 2
WAS (matrix
WAS  [ 2.0, 0.0, 0.0, 0.0
WAS  , 0.0, 2.0, 0.0, 0.0
WAS  , 0.0, 0.0, 2.0, 0.0
WAS  , 0.0, 0.0, 0.0, 1.0 ] :: L 4 4)
NOW (matrix
 [ 2.0, 0.0, 0.0, 0.0
 , 0.0, 2.0, 0.0, 0.0
 , 0.0, 0.0, 2.0, 0.0
 , 0.0, 0.0, 0.0, 1.0 ] :: L 4 4)

>>> scaleMat4 2 2 2 `mul` translationMatrix (vec3 1 1 1)
WAS (matrix
WAS  [ 2.0, 0.0, 0.0, 2.0
WAS  , 0.0, 2.0, 0.0, 2.0
WAS  , 0.0, 0.0, 2.0, 2.0
WAS  , 0.0, 0.0, 0.0, 1.0 ] :: L 4 4)
NOW (matrix
 [ 2.0, 0.0, 0.0, 2.0
 , 0.0, 2.0, 0.0, 2.0
 , 0.0, 0.0, 2.0, 2.0
 , 0.0, 0.0, 0.0, 1.0 ] :: L 4 4)

-}


{-|
>>> (translationMatrix (vec3 1 1 1) S.<> scaleMat4 2 2 2 ) #> vec4 1 1 1 1
WAS WAS Couldn't match expected type ‘L 4 n’ with actual type ‘R 4’
WAS NOW (vector [4.0,4.0,4.0,1.0] :: R 4)
NOW (vector [3.0,3.0,3.0,1.0] :: R 4)

-}

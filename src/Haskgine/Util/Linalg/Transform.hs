{-# LANGUAGE DataKinds, BangPatterns, RecordWildCards #-}
module Haskgine.Util.Linalg.Transform where

import GHC.TypeLits
import Numeric.LinearAlgebra.Static as S
import Haskgine.Util.Linalg.Matrices
import Haskgine.Util.Linalg.Vector

import Data.Default


class Rotatable3D a where
  
class Translatable3D a where

class Scalabe3D a where



-- | Entity tansform are a cllas transform style for entities in 3d space. (Position, scale,
-- rotation)
class HasTransform a where

  updateMatrix :: a -> a
  toTransform :: a -> Transform
  transformMatrix :: a -> L 4 4

  position :: a -> R 3
  position = _position . toTransform

  scale :: a -> R 3
  scale = _scale . toTransform

  rotation :: a -> R 3
  rotation = _rotation . toTransform

  withRotation :: a -> R 3 -> a
  withRotation a r = updateMatrix $ updateRotation  a (const r)

  updateRotation :: a -> (R 3 -> R 3) -> a
  updateRotation a f = withRotation a (f (rotation a))

  withXRotation :: a -> Double -> a
  withXRotation a r = updateRotation a (updateX (const r) )

  withYRotation :: a -> Double -> a
  withYRotation a r =  updateRotation a (updateY (const r))

  withZRotation :: a -> Double -> a
  withZRotation a r =  updateRotation a (updateZ (const r))

  updatePosition :: a -> (R 3 -> R 3) -> a
  updatePosition a f = updateMatrix $ withPosition a (f (position a))

  withPosition :: a -> R 3 -> a
  withPosition a v3 = updateMatrix $ updatePosition a (const v3)

  addPositon :: a -> R 3 -> a
  addPositon a v3 = updateMatrix $ updatePosition a (+ v3)

  withScale :: a -> R 3 -> a
  withScale a v = updateMatrix $ updateScale a (const v)

  updateScale :: a -> (R 3 -> R 3) -> a
  updateScale a f = updateMatrix $ withScale a (f (scale a))

  addScale :: a -> R 3 -> a
  addScale a v = updateMatrix $ updateScale a (+v)


  rotateHorizontal :: a -> ℝ -> a
  rotateHorizontal a rads =
    updateRotation a (updateX (+rads))

  rotateVertical :: a -> ℝ -> a
  rotateVertical a rads =   updateRotation a (updateY (+rads))

data Transform = Transform
                 { _transformMatrix :: L 4 4
                 , _position :: R 3
                 , _scale :: R 3
                 , _rotation :: R 3 -- pitch yaw roll
                 } deriving (Show)


instance HasTransform Transform where
  toTransform = id
  updateMatrix a@Transform{..} = a{
    _transformMatrix =
         translationMatrix _position
         S.<> rotateXMat4 (dimX _rotation)
         S.<> rotateYMat4 (dimY _rotation)
         S.<> rotateZMat4 (dimZ _rotation)
         S.<> scaleMat4 (dimX _scale) (dimY _scale) (dimZ _scale)
    }
  transformMatrix = _transformMatrix
  position = _position
  scale = _scale

  withPosition a v3 = updateMatrix $ a{_position = v3}
  withScale a v3 = updateMatrix $ a{_scale = v3}
  withRotation a v3 = updateMatrix $ a{_rotation = v3 }
  updateRotation a f = updateMatrix $ a{ _rotation = f (_rotation a) }

instance Default Transform where
  def = updateMatrix $ Transform
        { _transformMatrix = diag 1
        , _position = 0
        , _scale = 1
        , _rotation = vec3 0 0 0
        }

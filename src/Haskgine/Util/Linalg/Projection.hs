{-# LANGUAGE DataKinds, RecordWildCards  #-}
module Haskgine.Util.Linalg.Projection (
  HasProjection(..)
  , Perspective
  ) where

import GHC.TypeLits
import Numeric.LinearAlgebra.Static
import Haskgine.Util.Linalg.Matrices

import Data.Default


class HasProjection p where
  projection :: p -> L 4 4

  withAspect :: p -> Double -> p
  withNear :: p -> Double -> p
  withFar :: p -> Double -> p
  withFov :: p -> Double -> p

data Orthagonal

data Perspective =
  Perspective
  { _projection :: L 4 4
  , _fov  :: Double
  , _aspect :: Double
  , _near :: Double
  , _far  :: Double
  } deriving (Show)


makePerspective :: Double -> Double -> Double -> Double -> Perspective
makePerspective  fov aspect near far =
  updatePerspective $ Perspective undefined fov aspect near far

updatePerspective :: Perspective -> Perspective
updatePerspective a@Perspective{..} = a{_projection = perspectiveRH _fov _aspect _near _far}

instance HasProjection Perspective where
  projection = _projection

  withAspect p a = updatePerspective $ p{ _aspect = a}
  withNear p a = updatePerspective $ p{ _near = a}
  withFar p a = updatePerspective $ p{ _far = a}
  withFov p a = updatePerspective $ p{ _fov = a}

-- | Not a very useful default data type but can be used for debugging
instance Default Perspective where
  def = makePerspective (3*pi/4) (16/9) 0.01 100

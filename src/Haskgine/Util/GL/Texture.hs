-- | Author: Tomas Möre 2019
-- Thus file contains some haskellifications uppon the texture bindings in opengl.
{-# LANGUAGE FlexibleContexts, MultiParamTypeClasses,  FunctionalDependencies, TypeFamilies, TypeFamilyDependencies, ScopedTypeVariables, TypeOperators, DataKinds, TypeInType #-}
module Haskgine.Util.GL.Texture (
  Texture2D
  , Texture3D
  , IsTextureObject(..)
  , texSize2D
  , texSize3D
  , newTexture
  , newTextureWithData
  , bindToUniform
  , bindTexture
  , unbindTexture
  -- | Re exporting useful GL structures
  , GL.Proxy(..)
  , GL.Level
  , GL.PixelInternalFormat(..)
  , GL.TextureSize2D(..)
  , GL.TextureSize3D(..)
  , GL.TextureTarget2D(..)
  , GL.TextureTarget3D(..)
  , GL.Border
  , GL.PixelData(..)
  , GL.FramebufferTarget(..)
  , GL.FramebufferObjectAttachment(..)
  , GL.MinificationFilter
  , GL.MagnificationFilter
  , GL.TextureFilter(..)
  , GL.TextureUnit(..)
  , nullPtr
  ) where

import Foreign.Ptr (nullPtr)

import Control.Monad.IO.Class

import qualified Graphics.Rendering.OpenGL as GL
import qualified Graphics.GL as GLRaw

import Data.StateVar
import Data.Maybe

newtype Texture2D = Texture2D GL.TextureObject
newtype Texture3D = Texture3D GL.TextureObject

texSize2D :: Integral d => d -> d -> GL.TextureSize2D
texSize2D width height = GL.TextureSize2D (fromIntegral width) (fromIntegral height)

texSize3D :: Integral d => d -> d -> d -> GL.TextureSize3D
texSize3D width height depth = GL.TextureSize3D (fromIntegral width) (fromIntegral height) (fromIntegral depth)

class (GL.BindableTextureTarget (TextureTarget t), GL.ParameterizedTextureTarget (TextureTarget t)) =>  IsTextureObject t where
  type TextureSize t = a | a -> t
  type TextureTarget t = a | a -> t

  toTextureObject :: t -> GL.TextureObject
  fromTextureObject :: GL.TextureObject -> t
  texImage :: MonadIO m => t -> GL.Proxy -> GL.Level -> GL.PixelInternalFormat -> (TextureSize t) -> GL.Border -> GL.PixelData a -> m ()
  texTarget :: t -> TextureTarget t

  framebufferTexture :: (MonadIO m) => t -> GL.FramebufferTarget -> GL.FramebufferObjectAttachment -> TextureTarget t -> GL.Level -> m ()

instance IsTextureObject Texture2D  where
  type instance TextureTarget Texture2D = GL.TextureTarget2D
  type instance TextureSize Texture2D = GL.TextureSize2D

  toTextureObject (Texture2D t) = t
  fromTextureObject = Texture2D
  texImage _ a b c d e f = liftIO $ GL.texImage2D GL.Texture2D a b c d e f
  texTarget _ = GL.Texture2D

  framebufferTexture texture fbTarget fbObjAttachment target lvl =
    liftIO $ GL.framebufferTexture2D fbTarget fbObjAttachment target (toTextureObject texture) lvl

instance IsTextureObject Texture3D where
  type instance TextureTarget Texture3D = GL.TextureTarget3D
  type instance TextureSize Texture3D = GL.TextureSize3D

  toTextureObject (Texture3D t) = t
  fromTextureObject = Texture3D
  texImage _ a b c d e f = liftIO $ GL.texImage3D GL.Texture3D a b c d e f
  texTarget _ = GL.Texture3D

  framebufferTexture texture fbTarget fbObjAttachment target lvl =
    liftIO $ GL.framebufferTexture3D fbTarget fbObjAttachment target (toTextureObject texture) lvl 0

-- | Note to self, all textures created like this does not work
newTexture :: (IsTextureObject t, MonadIO m) => (TextureSize t) -> GL.PixelInternalFormat -> GL.MinificationFilter -> GL.MagnificationFilter -> m t
newTexture  texSize pixelType min mag =
  newTextureWithData (texTarget proxy) texSize pixelType min mag Nothing
  where
    proxy = undefined :: t

newTextureWithData :: (IsTextureObject t, MonadIO m) => (TextureTarget t) -> (TextureSize t) -> GL.PixelInternalFormat -> GL.MinificationFilter -> GL.MagnificationFilter -> Maybe (GL.PixelData a) -> m t
newTextureWithData tTarget texSize pixelType min mag mPixelData = liftIO $ do
  tex <- GL.genObjectName
  GL.textureBinding tTarget $=! Just tex
  texImage proxy
           GL.NoProxy
           0
           pixelType
           texSize
           0
           (fromMaybe defPD mPixelData)
  GL.textureFilter tTarget $=! (min, mag)

  -- Note to self, this should be added to parameters
  GL.textureWrapMode tTarget GL.S $=! (GL.Repeated, GL.ClampToEdge)
  GL.textureWrapMode tTarget GL.T $=! (GL.Repeated, GL.ClampToEdge)


  pure $ fromTextureObject tex
  where
    proxy :: t
    proxy = undefined
    defPD = (GL.PixelData GL.RGB GL.Float nullPtr)


-- texImage :: MonadIO m => t -> GL.Proxy -> GL.Level -> GL.PixelInternalFormat -> (TextureSize t) -> GL.Border -> GL.PixelData a -> m ()
-- | Keeping these in case fancy types turns out to be too troublesome

-- newTexture2D  :: MonadIO m => GL.TextureSize2D -> GL.PixelInternalFormat -> GL.MinificationFilter -> GL.MagnificationFilter -> m Texture2D
-- newTexture2D texSize pixelType minFilter magFilter  =
--   newTexture2DWithData texSize pixelType minFilter magFilter Nothing

-- newTexture2DWithData :: MonadIO m => GL.TextureSize2D -> GL.PixelInternalFormat -> GL.MinificationFilter -> GL.MagnificationFilter -> Maybe (GL.PixelData a) -> m Texture2D
-- newTexture2DWithData texSize pixelType min mag mPixelData = liftIO $ do
--       tex <- GL.genObjectName
--       -- GL.activeTexture $= GL.TextureUnit 0
--       GL.textureBinding GL.Texture2D  $=! Just tex
--       GL.textureFilter GL.Texture2D $=! (min, mag)

--       GL.texImage2D GL.Texture2D GL.NoProxy
--                               0
--                               pixelType
--                               texSize
--                               0
--                               (fromMaybe defPD mPixelData)
--       pure $ Texture2D tex
--   where
--     defPD = (GL.PixelData GL.RGB GL.Float nullPtr)


bindTexture :: (IsTextureObject t, MonadIO m) => t -> m ()
bindTexture t = GL.textureBinding (texTarget t) $=! Just (toTextureObject t)

unbindTexture :: (IsTextureObject t, MonadIO m) => t -> m ()
unbindTexture t = GL.textureBinding (texTarget t) $=! Nothing

--  Super note note self. GL.activeTexture $=! tu is not the same as activeTexture
bindToUniform :: (IsTextureObject t, MonadIO m) => t -> GL.TextureUnit -> GL.UniformLocation -> m ()
bindToUniform tex tu@(GL.TextureUnit n) (GL.UniformLocation loc) = liftIO $ do
  GLRaw.glUniform1i loc (fromIntegral n)
  GL.activeTexture $=! tu
  bindTexture tex

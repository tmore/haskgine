-- | Author: Tomas Möre 2019
-- This library contains utility functions for creating simple textures that has a single color.
-- as well as a set of functions to create the most basic colors around.
{-# LANGUAGE OverloadedLists #-}
module Haskgine.Util.GL.TextureColor
  ( Texture2D
  , newColorTextureRGBAF
  , newColorTextureRGBF
  , newColorTextureGreyF

  , newRedTexture
  , newGreenTexture
  , newBlueTexture
  , newBlackTexture
  , newWhiteTexture
  , newMagentaTexture
  , newYellowTexture
  , newCyanTexture
  ) where

import qualified Graphics.Rendering.OpenGL as GL
import Haskgine.Util.GL.Texture

import Control.Monad.IO.Class

import qualified Data.Vector.Storable as SV

-- | Constucts a texture of size 1 x 1 pixels. Used as a standin for
newColorTextureRGBAF :: (Real f, MonadIO m) => f -> f -> f -> f -> m Texture2D
newColorTextureRGBAF r g b a = liftIO $ do
  let vec = [realToFrac r, realToFrac g, realToFrac b, realToFrac a] :: SV.Vector Float
  SV.unsafeWith vec $ \ ptr ->
    newTextureWithData (texTarget proxy)
                       (TextureSize2D 1 1)
                       RGBA'
                       (Nearest, Nothing)
                       Nearest
                       (Just (PixelData GL.RGBA GL.Float ptr))
  where
    proxy = undefined :: t

newColorTextureRGBF :: (Real f, MonadIO m) => f -> f -> f -> m Texture2D
newColorTextureRGBF r g b = newColorTextureRGBAF r g b 1

newColorTextureGreyF :: (Real f,MonadIO m) => f -> m Texture2D
newColorTextureGreyF s = newColorTextureRGBF s s s

newRedTexture :: MonadIO m => m Texture2D
newRedTexture = newColorTextureRGBAF 1 0 0 (1 :: Float)

newGreenTexture :: MonadIO m => m Texture2D
newGreenTexture = newColorTextureRGBAF 0 1 0 (1 :: Float)

newBlueTexture :: MonadIO m => m Texture2D
newBlueTexture = newColorTextureRGBAF 0 0 1 (1 :: Float)

newBlackTexture :: MonadIO m => m Texture2D
newBlackTexture = newColorTextureRGBAF 0 0 0 (1 :: Float)

newWhiteTexture :: MonadIO m => m Texture2D
newWhiteTexture = newColorTextureRGBAF 1 1 1 (1 :: Float)

newMagentaTexture :: MonadIO m => m Texture2D
newMagentaTexture = newColorTextureRGBAF 1 0 1 (1 :: Float)

newYellowTexture :: MonadIO m => m Texture2D
newYellowTexture = newColorTextureRGBAF 1 1 0 (1 :: Float)

newCyanTexture :: MonadIO m => m Texture2D
newCyanTexture = newColorTextureRGBAF 1 1 0 (1 :: Float)

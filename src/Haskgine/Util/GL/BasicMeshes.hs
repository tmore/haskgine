{-# LANGUAGE OverloadedLists #-}
module Haskgine.Util.GL.BasicMeshes (
  module Haskgine.Util.GL.Mesh
  , unitQuad
  , unitCube
  ) where

import Graphics.Rendering.OpenGL as GL

import Haskgine.Util.GL.Mesh


unitQuad :: Mesh
unitQuad = Mesh
           { vertices = [ -0.5,  0.5, 0
                        ,  0.5,  0.5, 0
                        ,  0.5, -0.5, 0
                        , -0.5, -0.5, 0]
           , indices = [0, 1, 3
                       ,1, 2, 3]
           , normals = [0, 0, 1
                       ,0, 0, 1
                       ,0, 0, 1
                       ,0, 0, 1]
           , uv = []
           , primitiveMode = GL.Triangles
           }


-- | Unit cube mesh
-- Points taken from learnopengl
unitCube :: Mesh
unitCube = Mesh {
  vertices = [

      0.5,  0.5, -0.5,
      0.5, -0.5, -0.5,
      -0.5, -0.5, -0.5,
      -0.5, -0.5, -0.5,
      -0.5,  0.5, -0.5,
      0.5,  0.5, -0.5,

        -0.5, -0.5,  0.5,
      0.5, -0.5,  0.5,
      0.5,  0.5,  0.5,
        0.5,  0.5,  0.5,
      -0.5,  0.5,  0.5,
        -0.5, -0.5,  0.5,

        -0.5,  0.5,  0.5,
      -0.5,  0.5, -0.5,
      -0.5, -0.5, -0.5,
        -0.5, -0.5, -0.5,
      -0.5, -0.5,  0.5,
        -0.5,  0.5,  0.5,

      0.5, -0.5, -0.5,
      0.5,  0.5, -0.5,
        0.5,  0.5,  0.5,
      0.5,  0.5,  0.5,
      0.5, -0.5,  0.5,
        0.5, -0.5, -0.5,

        -0.5, -0.5, -0.5,
      0.5, -0.5, -0.5,
      0.5, -0.5,  0.5,
        0.5, -0.5,  0.5,
      -0.5, -0.5,  0.5,
        -0.5, -0.5, -0.5,

      0.5,  0.5,  0.5,
      0.5,  0.5, -0.5,
        -0.5,  0.5, -0.5,
      -0.5,  0.5, -0.5,
      -0.5,  0.5,  0.5,
        0.5,  0.5,  0.5
      ]
    , indices = [0,1,2, 3,4,5, 6,7,8, 9,10,11, 12,13,14, 15,16,17, 18,19,20, 21,22,23, 24,25,26, 27,28,29, 30,31,32, 33,34,35]
    , normals = [ 0,  0, -1
                 ,0,  0, -1
                 ,0,  0, -1
                 ,0,  0, -1
                 ,0,  0, -1
                 ,0,  0, -1

                 ,0,  0,  1
                 ,0,  0,  1
                 ,0,  0,  1
                 ,0,  0,  1
                 ,0,  0,  1
                 ,0,  0,  1

                 ,-1,  0,  0
                 ,-1,  0,  0
                 ,-1,  0,  0
                 ,-1,  0,  0
                 ,-1,  0,  0
                 ,-1,  0,  0

                 ,1,  0,  0
                 ,1,  0,  0
                 ,1,  0,  0
                 ,1,  0,  0
                 ,1,  0,  0
                 ,1,  0,  0

                 ,0, -1,  0
                 ,0, -1,  0
                 ,0, -1,  0
                 ,0, -1,  0
                 ,0, -1,  0
                 ,0, -1,  0

                 ,0,  1,  0
                 ,0,  1,  0
                 ,0,  1,  0
                 ,0,  1,  0
                 ,0,  1,  0
                 ,0,  1,  0
                ]
    , uv = []
    , primitiveMode = GL.Triangles
    }

-- | Mesh or a simple unit cube without UV coordinates
-- This old vesion was uncapable o containing the proper normals, keeping it here in case i need it some day. Like a squrrels nut
-- unitCube :: Mesh
-- unitCube = Mesh
--            { vertices = [ -1, 1, 1,
--                          1, 1, 1,
--                          1, 1,-1,
--                          -1, 1,-1,
--                          -1,-1, 1,
--                          1,-1, 1,
--                          1,-1,-1,
--                          -1,-1,-1
--                        ]
--            , indices = [ 7, 3, 2,
--                          7, 2, 6,
--                          6, 2, 1,
--                          6, 1, 5,
--                          5, 1, 0,
--                          5, 0, 4,
--                          4, 0, 3,
--                          4, 3, 7,
--                          3, 0, 1,
--                          3, 1, 2,
--                          7, 5, 4,
--                          7, 6, 5
--                        ]
--            , normals = [ 0,0,-1,
--                          0,0,-1,
--                          1,0,0,
--                          1,0,0,
--                          0,0,1,
--                          0,0,1,
--                          -1,0,0,
--                          -1,0,0,
--                          0,1,0,
--                          0,1,0,
--                          0,-1,0,
--                          0,-1,0
--                        ]
--            , uv = []
--            , primitiveMode = GL.Triangles
--            }

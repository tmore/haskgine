{-# LANGUAGE DataKinds #-}
module Haskgine.Util.GL.ColorTextures where

import Numeric.LinearAlgebra.Static

import Graphics.Rendering.OpenGL as GL
import Data.Vector.Storable as SV

red   = vec3 1 0 0
green = vec3 0 1 0
blue  = vec3 0 0 1
magenta = vec3 1 0 1


createSingleColorTexture :: R 3 -> IO TextureObject
createSingleColorTexture vec = do
  texture <- genObjectName
  textureBinding Texture2D $=! pure texture

  SV.unsafeWith (unwrap vec) $ (\ ptr -> do
    let size = (TextureSize2D 1 1)
        pixelData = PixelData GL.RGBA GL.Float ptr
    texImage2D Texture2D Proxy 0 GL.RGBA8 size 0 pixelData
                               )
  textureBinding Texture2D $=! Nothing
  pure texture

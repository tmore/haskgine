module Haskgine.Util.GL.Mesh where

import Data.Vector.Storable
import Numeric.LinearAlgebra.Static

import Graphics.Rendering.OpenGL as GL

import Data.Int

data Mesh = Mesh
            { vertices       :: Vector Float
            , indices        :: Vector Int32
            , normals        :: Vector Float
            , uv             :: Vector Float
            , primitiveMode  :: GL.PrimitiveMode
            } deriving (Show, Eq)

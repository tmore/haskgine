{-# LANGUAGE RecordWildCards #-}

module Haskgine.Util.GL.VAO (
  fromMesh
  , VAO
  , draw
  , ShaderLayout(..)
  ) where

import Graphics.Rendering.OpenGL as GL

import Data.Int
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as SV

import Foreign.Storable
import Foreign.Ptr (nullPtr)
import Foreign.C.Types

import Control.Monad (when)
import Control.Monad.IO.Class

import Haskgine.Util.GL.Mesh (Mesh)
import Haskgine.Util.GL.Mesh as Mesh

import Data.Default

data VAO = VAO
           { vao           :: VertexArrayObject
           , numElements   :: Int32
           , primitiveMode :: GL.PrimitiveMode
           } deriving (Show, Eq)

draw :: MonadIO m => VAO -> m ()
draw VAO{..} = liftIO $ do
  bindVertexArrayObject $=! pure vao
  drawElements primitiveMode numElements GL.UnsignedInt nullPtr

data ShaderLayout = ShaderLayout
                   { vertexLocation :: GL.AttribLocation
                   , normalLocation :: GL.AttribLocation
                   , uvLocation     :: GL.AttribLocation
                   } deriving (Show, Eq)

instance Default ShaderLayout where
  def = ShaderLayout (GL.AttribLocation 0) (GL.AttribLocation 1) (GL.AttribLocation 2)

fromMesh :: MonadIO m => Mesh -> ShaderLayout -> m VAO
fromMesh Mesh{..} ShaderLayout{..} = liftIO $ do
  vao <- genObjectName
  GL.bindVertexArrayObject $=! pure vao

  -- Inserts the vertives
  createArrayBuffer vertexLocation vertices
  GL.vertexAttribPointer vertexLocation $=! (ToFloat, VertexArrayDescriptor 3 Float 0 nullPtr)

  when (not $ SV.null normals) $ do
     createArrayBuffer normalLocation normals
     GL.vertexAttribPointer normalLocation $=! (ToFloat, VertexArrayDescriptor 3 Float 0 nullPtr)

  when (not $ SV.null uv) $ do
    createArrayBuffer uvLocation uv
    GL.vertexAttribPointer uvLocation $=! (ToFloat, VertexArrayDescriptor 2 Float 0 nullPtr)

  createStaticBuffer GL.ElementArrayBuffer indices

  GL.bindVertexArrayObject $=! Nothing
  GL.bindBuffer ArrayBuffer $=! Nothing
  pure $ VAO vao (fromIntegral $ SV.length indices) primitiveMode

createArrayBuffer :: (Storable a) => GL.AttribLocation -> Vector a ->  IO ()
createArrayBuffer location vec = do
  createStaticBuffer GL.ArrayBuffer vec
  GL.vertexAttribArray location $= GL.Enabled

createStaticBuffer :: (Storable a) => BufferTarget -> Vector a -> IO ()
createStaticBuffer target vec = do
  buffer <- genObjectName :: IO BufferObject
  GL.bindBuffer target $=! pure buffer
  SV.unsafeWith vec $ (\ ptr -> do
                          GL.bufferData target $=! (
                            CPtrdiff $ fromIntegral (sizeOf (SV.head vec) * SV.length vec)
                            , ptr
                            , GL.StaticDraw
                            )
                          pure ()
                      )

{-# LANGUAGE ScopedTypeVariables, FlexibleContexts #-}
module Haskgine.Util.GL.Program where

import qualified Graphics.Rendering.OpenGL as GL
import Haskgine.Util.GL.Shader

import Util.Errors (liftError)

import Control.Monad
import Control.Monad.Except

import Data.StateVar (($=!), get)

-- | General data class for reporting errors whilst creating shader
-- programs. MissingUniformError should be created by extentions
data ProgramCreationError = ProgramMissingUniformError String
                          | ProgramLinkError String
                          | ProgramShaderError ShaderCreationError
                          deriving (Show, Read, Eq)

class HasGLProgram p where
  toGLProgram :: p -> GL.Program

instance HasGLProgram GL.Program where
  toGLProgram = id

bindProgram :: (MonadIO m, HasGLProgram p) => p -> m ()
bindProgram = (GL.currentProgram $=!) . Just . toGLProgram

unbindProgram :: MonadIO m => m ()
unbindProgram = GL.currentProgram $=! Nothing

withGLProgram :: (MonadIO m, HasGLProgram p) => p -> m a -> m a
withGLProgram program action = do
  bindProgram program
  res <- action
  unbindProgram
  pure res

makeGLProgram :: (MonadError ProgramCreationError m, MonadIO m) => VertexShader -> FragmentShader -> Maybe GeometryShader -> m GL.Program
makeGLProgram vertex fragment mGeomerty = do
  program <- liftIO GL.createProgram
  GL.attachedShaders program $=! (toGLShader vertex
                               : toGLShader fragment
                               : maybe [] (pure . toGLShader) mGeomerty)
  liftIO $ GL.linkProgram program

  linked <- get (GL.linkStatus program)
  when (not linked) $ do
    error <- liftIO $ get (GL.programInfoLog program)
    throwError (ProgramLinkError error)
  pure program

liftShaderError :: (MonadError ProgramCreationError m, MonadIO m) => m (Either ShaderCreationError a) -> m a
liftShaderError = liftError ProgramShaderError

fromSourceFiles :: (MonadError ProgramCreationError m, MonadIO m) => FilePath -> FilePath -> Maybe FilePath -> m GL.Program
fromSourceFiles vertexFp fragmentFp maybeGeometryFp = do
  vertexShader <- liftShaderError (runExceptT $ fromSourceFile vertexFp)
  fragmentShader <- liftShaderError (runExceptT $ fromSourceFile fragmentFp)
  maybeGeometryShader <- liftShaderError (runExceptT $ case maybeGeometryFp of
                           Nothing -> pure Nothing
                           Just geometryFp ->
                             (Just <$> fromSourceFile geometryFp))
  makeGLProgram vertexShader fragmentShader maybeGeometryShader
  where
    toErr a = do
      e <- a
      case e of
        Left err -> throwError (ProgramShaderError err)
        Right c -> pure c

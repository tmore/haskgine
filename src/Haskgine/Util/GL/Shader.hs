-- | Author: Tomas Möre 2019
{-# LANGUAGE ScopedTypeVariables, FlexibleContexts #-}
module Haskgine.Util.GL.Shader (
  ShaderCreationError(..),
  VertexShader,
  FragmentShader,
  GeometryShader,
  glShaderType,
  createShader,
  deleteShader,
  toGLShader,
  fromSourceFile

  )
where


import System.Directory

import Control.Monad
import Control.Monad.Except

import qualified Graphics.Rendering.OpenGL as GL
import Data.ByteString as BS
import Data.StateVar (($=!), get)


data ShaderCreationError = MissingFileError FilePath
                         | CompilationError String
                         deriving (Show, Read, Eq)


newtype VertexShader = VertexShader GL.Shader
newtype FragmentShader = FragmentShader GL.Shader
newtype GeometryShader = GeometryShader GL.Shader

class Shader s where
  glShaderType :: s -> GL.ShaderType

  _shaderConstructor :: s -> (GL.Shader -> s)
  toGLShader :: s -> GL.Shader

  createShader :: (MonadError ShaderCreationError m, MonadIO m) => ByteString -> m  s
  createShader src = do
    let proxy = undefined :: s
        shaderType = glShaderType proxy
    shader <- liftIO (GL.createShader shaderType)
    liftIO $ GL.shaderSourceBS shader $=! src
    liftIO $ GL.compileShader shader
    shaderStatusOk <- liftIO $ GL.compileStatus shader
    when (not shaderStatusOk) $ do
      errorMsg <- get $ GL.shaderInfoLog shader
      throwError (CompilationError errorMsg)
    pure $ _shaderConstructor proxy shader

  deleteShader :: MonadIO m => s -> m ()
  deleteShader s = do
    let shader = toGLShader s
    GL.deleteObjectName shader


fromSourceFile :: (MonadError ShaderCreationError m, Shader s, MonadIO m) => FilePath -> m s
fromSourceFile fp =  do
  fileExists <- liftIO $ doesFileExist fp
  when (not fileExists) $ throwError (MissingFileError fp)
  sourceBS <- liftIO $ BS.readFile fp
  createShader sourceBS

instance Shader VertexShader where
  glShaderType = const GL.VertexShader
  _shaderConstructor _ = VertexShader
  toGLShader (VertexShader s) = s

instance Shader FragmentShader where
  glShaderType = const GL.FragmentShader
  _shaderConstructor _ = FragmentShader
  toGLShader (FragmentShader s) = s


instance Shader GeometryShader where
  glShaderType = const GL.GeometryShader
  _shaderConstructor _ = GeometryShader
  toGLShader (GeometryShader s) = s

{-# LANGUAGE DataKinds, ScopedTypeVariables, FlexibleContexts #-}

module Haskgine.Util.GL.Uniform where

import qualified Graphics.GL as GLRaw
import Graphics.Rendering.OpenGL as GL
import qualified Data.Vector.Storable as SV

import Numeric.LinearAlgebra.Static
import Numeric.LinearAlgebra (flatten)

import Haskgine.Util.GL.Program (ProgramCreationError(..))

import Control.Monad.IO.Class
import Control.Monad.Except

-- | Note, should probably be set to use the OpenGL librarys uniform class
setMatrixUniform :: MonadIO m => UniformLocation -> L 4 4 -> m ()
setMatrixUniform (UniformLocation loc) mat = do
  let mat_vec = SV.map realToFrac $ flatten $ extract mat
  liftIO $ SV.unsafeWith mat_vec $ \ ptr -> do
    GLRaw.glUniformMatrix4fv loc 1 GLRaw.GL_TRUE ptr

setVector3Uniform :: MonadIO m => UniformLocation -> R 3 -> m ()
setVector3Uniform (UniformLocation loc) v = do
  let vec = SV.map realToFrac $ extract v
  liftIO $ SV.unsafeWith vec $ \ ptr -> do
    GLRaw.glUniform3fv loc 1 ptr

-- | Attempts to fetch a uniform, return an error with the name of the missing shader
getUniformLocation :: (MonadError ProgramCreationError m, MonadIO m) => GL.Program -> String -> m UniformLocation
getUniformLocation program name = do
  loc <- liftIO $ uniformLocation program name
  when (notFound loc) $ throwError (ProgramMissingUniformError name)
  pure loc
  where
    notFound (UniformLocation (-1)) = True
    notFound _ = False

-- | Attempts to fetch a uniform if the uniform isn't found the -1 uniform is returned instead as per opengl standar
getUniformLocationOptional :: (MonadError ProgramCreationError m, MonadIO m) => GL.Program -> String -> m UniformLocation
getUniformLocationOptional program name =
  liftIO $ uniformLocation program name

module Haskgine.Scene.Entities where

import Haskgine.Graphics.Entity (Entity)
import qualified Haskgine.Graphics.Entity as Entity

newtype StaticEntity = StaticEntity Entity

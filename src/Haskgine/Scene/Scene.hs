-- | Author: Tomas Möre 2019
{-# LANGUAGE DataKinds, RecordWildCards #-}
module Haskgine.Scene.Scene where

import Numeric.LinearAlgebra.Static

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HM

import Haskgine.Util.Resources.Store (ResourceStore)
import qualified Haskgine.Util.Resources.Store as RS
import qualified Haskgine.Util.Resources.Util as RS

import Haskgine.Graphics.Entity (Entity)
import Haskgine.Graphics.Camera (Camera)

import Haskgine.Renderer (Renderer)
import qualified Haskgine.Renderer as Renderer

import Data.Text (Text)

import Data.Vector (Vector)

import Data.Foldable
import Haskgine.Util.GL.VAO (VAO)
import Haskgine.Util.GL.Texture (Texture2D)

import Control.Concurrent.MVar

import Control.Monad.IO.Class

-- | Currently scene is put into for a better name.
-- Note to self, internal storage should probably be linked with Weak memory references instead.
data Scene = Scene
             { _globalShadowDir :: MVar (R 3)
             , _lights          :: MVar (Vector (R 3))
             , _entities        :: MVar (ResourceStore (MVar Entity))
             , _baseEntities    :: MVar (HashMap Text Entity)
             , _textures        :: MVar (HashMap Text Texture2D)
             , _vaos            :: MVar (HashMap Text VAO)
             }

type EntityKey = RS.ResourceKey (MVar Entity)

data Recipe m a = Recipe Text (m a)

type EntityRecipe m = [Recipe m Texture2D] -> [Recipe m VAO] -> (HashMap Text Texture2D -> HashMap Text VAO -> m Entity)

withEntities :: MonadIO m => Scene -> ([Entity] -> m a) -> m a
withEntities Scene{..} action  = do
  varEntities <- liftIO $ takeMVar _entities
  entities <- traverse (liftIO . readMVar) $ RS.toList varEntities
  res <- action entities
  liftIO $ putMVar _entities varEntities
  pure res

-- | Constructs a new entity, and puts it into the scene

-- Note to self, this process can be made a bit more efficent and
-- parallelisable. For example implementing some kind of set containing info
-- about what objects are currently beeing constructed with allow for
-- parralellisation.
addEntityToScene :: (MonadIO m) =>
                    Scene ->
                    Text ->
                    [Recipe m Texture2D] ->
                    [Recipe m VAO] ->
                    (HashMap Text Texture2D -> HashMap Text VAO -> m Entity) ->
                    m (EntityKey, MVar Entity)
addEntityToScene Scene{..} name texturesR vaoR entityRecipe = do
  baseEntities <- liftIO $ takeMVar _baseEntities
  case HM.lookup name baseEntities  of
    Just entity -> liftIO $ putMVar _baseEntities baseEntities >> makeVarEntity entity
    Nothing -> do
      texMap <- foldlM (\ texMap' (Recipe textureName constructFunc) -> do
                           t <- condInsert _textures textureName constructFunc
                           pure (HM.insert textureName t texMap')) mempty texturesR
      vaoMap <- foldlM (\ vaoMap' (Recipe vaoName constructFunc) -> do
                           t <- condInsert _vaos vaoName constructFunc
                           pure (HM.insert vaoName t vaoMap')) mempty vaoR
      entity <- entityRecipe texMap vaoMap
      liftIO $ putMVar _baseEntities (HM.insert name entity baseEntities)
      makeVarEntity entity
    where
      condInsert varStore key constructFunc = do
        store <- liftIO $ takeMVar varStore
        case HM.lookup key store of
          Just e -> liftIO (putMVar varStore store) >> pure e
          Nothing -> do
            a <- constructFunc
            liftIO $ putMVar varStore $ HM.insert key a store
            pure a

      makeVarEntity entity = liftIO $ do
        varEntities <- takeMVar _entities
        varEntity <- newMVar entity
        let (key, entities') = RS.insert varEntities varEntity
        putMVar _entities entities'
        pure (key, varEntity)

removeEntityFromScene :: (MonadIO m) => Scene -> EntityKey -> m ()
removeEntityFromScene Scene{..} key = do
  varEntities <- liftIO $ takeMVar _entities
  liftIO $ putMVar _entities (RS.delete key varEntities)

class MonadIO m => HasScene m where
  askScene :: m Scene

  addEntity :: Text -> [Recipe m Texture2D] -> [Recipe m VAO] -> (HashMap Text Texture2D -> HashMap Text VAO -> m Entity) -> m (EntityKey, MVar Entity)
  addEntity name txts vaos recipe = do
    scene <- askScene
    addEntityToScene scene name txts vaos recipe

  removeEntity :: EntityKey -> m ()
  removeEntity key = do
    scene <- askScene
    removeEntityFromScene scene key

newScene :: MonadIO m => m Scene
newScene = liftIO $ do
  globalShadowDirRef <- newMVar $ vec3 0 (-1) 0
  lightsRef <- newMVar mempty
  entitiesRef <- newMVar RS.empty
  baseEntitiesRef <- newMVar mempty
  texturesRef <- newMVar mempty
  vaosRef <- newMVar mempty
  pure Scene
        { _globalShadowDir = globalShadowDirRef
        , _lights          = lightsRef
        , _entities        = entitiesRef
        , _baseEntities    = baseEntitiesRef
        , _textures        = texturesRef
        , _vaos            = vaosRef
        }

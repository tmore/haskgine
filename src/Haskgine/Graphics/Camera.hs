{-# LANGUAGE DataKinds, RecordWildCards #-}

module Haskgine.Graphics.Camera where

import Haskgine.Util.Linalg
    ( vec3,
      R,
      cross,
      lookAtMatrixRH,
      rotateMatrix3,
      rotateZMat3,
      rotateYMat3,
      rotateXMat3,
      rotateAroundM3,
      rotateAroundM4,
      HasProjection(..),
      Perspective )
import Numeric.LinearAlgebra.Static as S
    

import Haskgine.Util.Linalg as S (normalize)

import Data.Default ( Default(..) )

import qualified Graphics.Rendering.OpenGL as GL
import Control.Monad.IO.Class 

data Camera = Camera
  { _camProjection :: Perspective
  , _camPosition   :: R 3
  , _camUp         :: R 3
  , _camAt         :: R 3 
  } deriving (Show)


withCameraPosition :: Camera -> R 3 -> Camera
withCameraPosition cam@Camera{..} pos = cam{ _camPosition = pos }

updateCameraPosition :: Camera -> (R 3 -> R 3) -> Camera
updateCameraPosition cam@Camera{..} f = cam{ _camPosition = f _camPosition }

withCameraUp :: Camera -> R 3 -> Camera
withCameraUp cam up = cam{ _camUp = up }

withCameraAt :: Camera -> R 3 -> Camera
withCameraAt cam at = cam{ _camAt = at }

withCameraRotation :: Camera -> Double -> Double -> Double -> Camera
withCameraRotation cam@Camera{..} pitch yaw roll =
  cam{ _camAt = rot #> _camAt 
     , _camUp = rot #> _camUp
     }
  where
    rot = rotateAroundM3 (cross _camAt _camUp) pitch
      `S.mul` rotateAroundM3 (vec3 0  1 0) yaw
      `S.mul` rotateAroundM3 _camAt roll

cameraLookAtMat :: Camera -> L 4 4
cameraLookAtMat Camera{..} =
  lookAtMatrixRH _camPosition (_camPosition + S.normalize _camAt) _camUp


instance HasProjection Camera where
  projection = projection . _camProjection

  withAspect p a = p{ _camProjection = withAspect (_camProjection p) a}
  withNear p a = p{ _camProjection = withNear (_camProjection p) a }
  withFar p a = p{ _camProjection = withFar (_camProjection p) a }
  withFov p a = p{ _camProjection = withFov (_camProjection p) a }


instance Default Camera where
  def = Camera
    { _camProjection = def
    , _camPosition   = vec3 0 0 1
    , _camUp         = vec3 0 1 0
    , _camAt         = vec3 0 0 (-1)
    }

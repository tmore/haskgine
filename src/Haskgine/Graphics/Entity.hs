module Haskgine.Graphics.Entity (
  module Haskgine.Graphics.Entity.Entity
  , module Haskgine.Graphics.Entity.Shapes
                                ) where

import Haskgine.Graphics.Entity.Entity
import Haskgine.Graphics.Entity.Shapes

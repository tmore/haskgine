-- | Author: Tomas Möre 2019
{-# LANGUAGE DataKinds, FlexibleContexts #-}
module Haskgine.Graphics.Shaders.ShadowShader where

import Graphics.Rendering.OpenGL as GL

import Haskgine.Util.GL.Program
import Haskgine.Util.GL.Shader
import Haskgine.Util.GL.Uniform

import Numeric.LinearAlgebra.Static
import Control.Monad.Except

data ShadowProgram = ShadowProgram
                    { _modelUniform :: UniformLocation
                    , _lightProjection :: UniformLocation
                    , _shadowGLProgram :: GL.Program
                    } deriving (Show, Eq)

instance HasGLProgram ShadowProgram where
  toGLProgram = _shadowGLProgram

bindModelTransformation :: MonadIO m => ShadowProgram -> L 4 4 -> m ()
bindModelTransformation shadowShader mat =
  setMatrixUniform (_modelUniform shadowShader) $ mat

bindLightProjection :: MonadIO m => ShadowProgram -> L 4 4 -> m ()
bindLightProjection shadowShader mat =
  setMatrixUniform (_lightProjection shadowShader) $ mat

makeShadowProgram :: (MonadError ProgramCreationError m, MonadIO m) => VertexShader -> FragmentShader -> m ShadowProgram
makeShadowProgram vs fs = do
  program <- makeGLProgram vs fs Nothing
  withGLProgram program $ do
    lightProjection <- getUniformLocation program "light_projection"
    modelMatrix <- getUniformLocation program "model_trans"
    pure ShadowProgram
      { _modelUniform = modelMatrix
      , _lightProjection = lightProjection
      , _shadowGLProgram = program
      }

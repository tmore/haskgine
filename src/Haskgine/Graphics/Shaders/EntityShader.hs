{-# LANGUAGE DataKinds, ScopedTypeVariables, FlexibleContexts #-}
module Haskgine.Graphics.Shaders.EntityShader where

import Graphics.Rendering.OpenGL as GL

import Haskgine.Util.GL.VAO (ShaderLayout(..))
import Haskgine.Util.GL.Program
import Haskgine.Util.GL.Shader
import Haskgine.Util.GL.Uniform

import Numeric.LinearAlgebra.Static

import Control.Monad.Except

data EntityProgram = EntityProgram
                    { _modelUniform      :: UniformLocation
                    , _viewUniform       :: UniformLocation
                    , _projectionUniform :: UniformLocation
                    , _glProgram         :: GL.Program
                    , _shaderLayout      :: ShaderLayout
                    } deriving(Show, Eq)

instance HasGLProgram EntityProgram where
  toGLProgram = _glProgram

useView :: MonadIO m => EntityProgram -> L 4 4 -> m ()
useView es = setMatrixUniform (_viewUniform es)

useModel :: MonadIO m => EntityProgram -> L 4 4 -> m ()
useModel es = setMatrixUniform (_modelUniform es)

useProjection :: MonadIO m => EntityProgram -> L 4 4 -> m ()
useProjection es = setMatrixUniform (_projectionUniform es)

bindEntityTextures :: MonadIO m => TextureObject -> m ()
bindEntityTextures  diffuseTexture =
  mapM_ (\ (i, tex) -> do
            GL.activeTexture GL.$= GL.TextureUnit i
            GL.textureBinding GL.Texture2D $=! Just tex
           ) [(0, diffuseTexture)]

makeEntityProgram :: (MonadError ProgramCreationError m, MonadIO m) => VertexShader -> FragmentShader -> m EntityProgram
makeEntityProgram vertexShader fragmentShader = do
  program <- makeGLProgram vertexShader fragmentShader Nothing
  withGLProgram program $ do
    modelLocation <- getUniformLocation program "model"
    viewLocation <- getUniformLocation program "view"
    projectionLocation <- getUniformLocation program "projection"

    pure EntityProgram
      { _modelUniform = modelLocation
      , _viewUniform = viewLocation
      , _projectionUniform = projectionLocation
      , _glProgram = program
      , _shaderLayout = ShaderLayout (AttribLocation 0)
                                     (AttribLocation 1)
                                     (AttribLocation 2)
      }

-- | Author: Tomas Möre 2019
{-# LANGUAGE DataKinds, ScopedTypeVariables, FlexibleContexts, RecordWildCards #-}
module Haskgine.Graphics.Shaders.FinalizeShader where

import Graphics.Rendering.OpenGL as GL

import Haskgine.Util.GL.VAO (ShaderLayout(..), VAO)
import qualified Haskgine.Util.GL.VAO as VAO
import qualified Haskgine.Util.GL.BasicMeshes as Mesh
import Haskgine.Util.GL.Program
import Haskgine.Util.GL.Shader
import Haskgine.Util.GL.Uniform
import Haskgine.Util.GL.Texture

import Haskgine.Graphics.GBuffer

import Numeric.LinearAlgebra.Static
import Control.Monad.Except

data FinalizeProgram = FinalizeProgram
                    { _glProgram              :: GL.Program
                    , _albedoSpecTexUniform   :: UniformLocation
                    , _cameraPositionUniform  :: UniformLocation

                    , _globalLightPositionUniform  :: UniformLocation

                    , _lightProjectionUniform :: UniformLocation
                    , _positionTexUniform     :: UniformLocation
                    , _normalTexUniform       :: UniformLocation
                    , _shadowTexUniform       :: UniformLocation

                    , _quadVao                :: VAO
                    } deriving(Show, Eq)

instance HasGLProgram FinalizeProgram where
  toGLProgram = _glProgram

useGlobalLightPosition :: MonadIO m => FinalizeProgram -> R 3 -> m ()
useGlobalLightPosition FinalizeProgram{..} = setVector3Uniform _globalLightPositionUniform

useLightProjection :: MonadIO m => FinalizeProgram -> L 4 4 -> m ()
useLightProjection FinalizeProgram{..} = setMatrixUniform _lightProjectionUniform

useCameraPosition :: MonadIO m => FinalizeProgram -> R 3 -> m ()
useCameraPosition FinalizeProgram{..} = setVector3Uniform _cameraPositionUniform

-- | Addid this level of hardcoded dependence on a GBuffer should be avioded. Split into three diffrent functions
bindGBufferTextures :: MonadIO m => FinalizeProgram -> GBuffer -> m ()
bindGBufferTextures program@FinalizeProgram{..} gBuff = do
  bindProgram program
  bindPositionTex gBuff   (TextureUnit 0)  _positionTexUniform
  bindNormalTex  gBuff    (TextureUnit 1)  _normalTexUniform
  bindAlbedoSpecTex gBuff (TextureUnit 2) _albedoSpecTexUniform
  pure ()

bindShadowTexture :: MonadIO m => FinalizeProgram -> Texture2D -> m ()
bindShadowTexture program tex = do
  bindProgram program
  bindToUniform tex (TextureUnit 3) (_shadowTexUniform program)

draw :: MonadIO m => FinalizeProgram -> m ()
draw FinalizeProgram{..} = do
  VAO.draw _quadVao

makeFinalizeProgram :: (MonadError ProgramCreationError m, MonadIO m) => VertexShader -> FragmentShader -> m FinalizeProgram
makeFinalizeProgram vertexShader fragmentShader = do
  program <- makeGLProgram vertexShader fragmentShader Nothing
  withGLProgram program $ do

    cameraPositionUniform  <- getUniformLocation program "camera_position"
    lightProjectionUniform <- getUniformLocation program "light_projection"
    positionTexUniform     <- getUniformLocation program "position_tex"
    normalTexUniform       <- getUniformLocation program "normal_tex"
    albedoSpecTexUniform   <- getUniformLocation program "albedo_spec_tex"
    shadowTexUniform       <- getUniformLocation program "shadow_map_tex"
    globalLightPositionUniform <- getUniformLocation program "global_light_position"

    quadVao <- VAO.fromMesh Mesh.unitQuad (ShaderLayout
                                            (GL.AttribLocation 0)
                                            (GL.AttribLocation 1)
                                            (GL.AttribLocation 2))
    pure $ FinalizeProgram
      { _glProgram              = program

      , _lightProjectionUniform = lightProjectionUniform
      , _cameraPositionUniform  = cameraPositionUniform

      , _globalLightPositionUniform = globalLightPositionUniform

      , _positionTexUniform     = positionTexUniform
      , _normalTexUniform       = normalTexUniform
      , _albedoSpecTexUniform   = albedoSpecTexUniform

      , _shadowTexUniform       = shadowTexUniform
      , _quadVao                = quadVao
      }

{-# LANGUAGE DataKinds, RecordWildCards #-}
module Haskgine.Graphics.Entity.Entity where


import qualified Graphics.Rendering.OpenGL as GL
import Graphics.Rendering.OpenGL (($=!))

import Haskgine.Graphics.Shaders.EntityShader

import Haskgine.Util.GL.VAO as VAO
import Haskgine.Util.GL.Texture
import Haskgine.Util.Linalg.Transform as Transform


import Control.Monad.IO.Class

class IsEntity a where
  vao :: a -> VAO
  albedoTexture :: a -> Maybe Texture2D

  toEntity :: a -> Entity
  transform :: a -> Transform
  -- Minimal defenition:
  --- (toEntity) or (entityVao, entitiyAlbedoTexture, enityModelMatrix)
  toEntity a  = Entity (vao a) (albedoTexture a) (transform a)
  vao = _vao . toEntity
  albedoTexture = _albedoTexture . toEntity
  transform = _transform . toEntity


data Entity = Entity
              { _vao           :: VAO
              , _albedoTexture :: Maybe Texture2D
              , _transform     :: Transform
              }

instance IsEntity Entity where
  toEntity = id
  vao = _vao
  albedoTexture = _albedoTexture
  transform = _transform

instance HasTransform Entity where
  toTransform = _transform
  updateMatrix a = a{
    _transform = updateMatrix (_transform  a)
    }
  transformMatrix = transformMatrix . _transform

  withPosition a@Entity{..} v3 = a{ _transform = withPosition _transform v3}
  withScale a@Entity{..} v3 = a{_transform = withScale _transform v3}
  withRotation a@Entity{..} v3 = a{_transform = withRotation _transform v3}
  updateRotation a@Entity{..} f = a{_transform = updateRotation _transform f}

-- | Assumes the shader program is bound before call
draw :: (MonadIO m, IsEntity e) => EntityProgram -> e -> m ()
draw shader entity = do
  maybe (pure ())
        (\ tex -> do
            GL.activeTexture $=! GL.TextureUnit 0
            bindTexture tex
        )
        (albedoTexture entity)
--  useModel shader (transformMatrix (transform entity))
--  useModel shader eye -- (transformMatrix (transform entity))

  useModel shader $ transformMatrix $ transform entity

  VAO.draw (vao entity)
--  GL.activeTexture $=! (GL.TextureUnit 0)
--  GL.textureBinding GL.Texture2D $=! Nothing

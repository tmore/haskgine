module Haskgine.Graphics.Entity.Shapes where

import Haskgine.Util.GL.VAO (fromMesh, ShaderLayout(..))
import Haskgine.Util.GL.BasicMeshes (unitCube, unitQuad)
import Haskgine.Util.GL.Texture (Texture2D)
import Haskgine.Graphics.Entity.Entity

import Graphics.Rendering.OpenGL as GL

import Control.Monad.IO.Class

import Data.Default

defShaderLayouts = ShaderLayout (GL.AttribLocation 0)
                                (GL.AttribLocation 1)
                                (GL.AttribLocation 2)

makeUnitCube :: MonadIO m => Texture2D -> m Entity
makeUnitCube tex = liftIO $ do
  vao <- fromMesh unitCube defShaderLayouts
  pure Entity
    { _vao = vao
    , _albedoTexture = (Just tex)
    , _transform = def
    }

makeUnitQuad :: MonadIO m => Texture2D -> m Entity
makeUnitQuad tex = liftIO $ do
  vao <- fromMesh unitQuad defShaderLayouts
  pure Entity
    { _vao = vao
    , _albedoTexture = (Just tex)
    , _transform = def
    }

{-# LANGUAGE RecordWildCards, FlexibleContexts #-}
module Haskgine.Graphics.GBuffer where

import Haskgine.Util.GL.Texture

import Data.Word
import Data.StateVar

import Control.Monad.IO.Class
import Control.Monad
import Control.Monad.Except

import qualified Graphics.Rendering.OpenGL as GL

data GBuffer = GBuffer
  { _renderWidth       :: Word32
  , _renderHeight      :: Word32
  , _albedoSpecTexture :: Texture2D
  , _normalTexture     :: Texture2D
  , _positionTexture   :: Texture2D
  , _depthBuffer       :: GL.RenderbufferObject
  , _frameBuffer       :: GL.FramebufferObject
  }

deleteGBuffer :: MonadIO m => GBuffer -> m ()
deleteGBuffer GBuffer{..} = do
  GL.deleteObjectNames $ map toTextureObject [_albedoSpecTexture, _normalTexture, _positionTexture]
  GL.deleteObjectName _depthBuffer
  GL.deleteObjectName _frameBuffer

resizeGBuffer :: (MonadError String m, MonadIO m) => GBuffer -> Word32 -> Word32 -> m GBuffer
resizeGBuffer gBuffer newWidth newHeight = do
  deleteGBuffer gBuffer
  makeGBuffer newWidth newHeight

bindGBuffer :: MonadIO m => GBuffer -> m ()
bindGBuffer GBuffer{..} = GL.bindFramebuffer GL.Framebuffer $=! _frameBuffer

bindAlbedoSpecTex :: MonadIO m => GBuffer -> GL.TextureUnit -> GL.UniformLocation -> m ()
bindAlbedoSpecTex GBuffer{..} = bindToUniform  _albedoSpecTexture

bindNormalTex :: MonadIO m => GBuffer -> GL.TextureUnit -> GL.UniformLocation -> m ()
bindNormalTex GBuffer{..} = bindToUniform  _normalTexture

bindPositionTex :: MonadIO m => GBuffer -> GL.TextureUnit -> GL.UniformLocation -> m ()
bindPositionTex GBuffer{..} = bindToUniform  _positionTexture

-- | None to self, probably should add error checking to this
makeGBuffer :: (MonadError String m, MonadIO m) => Word32 -> Word32 -> m GBuffer
makeGBuffer width height =  do
  frameBuffer <- GL.genObjectName
  GL.bindFramebuffer GL.Framebuffer $=! frameBuffer

  positions <- newTextureWithData GL.Texture2D size GL.RGB16F minFilter magFilter (Just $ PixelData GL.RGB GL.Float nullPtr)
  framebufferTexture positions Framebuffer (ColorAttachment 0) Texture2D  0

  normals <- newTextureWithData GL.Texture2D size GL.RGB16F minFilter magFilter (Just $ PixelData GL.RGB GL.Float nullPtr)
  framebufferTexture  normals Framebuffer (ColorAttachment 1) Texture2D 0

  albedoSpec <- newTextureWithData GL.Texture2D size GL.RGBA' minFilter magFilter (Just $ PixelData GL.RGBA GL.UnsignedByte nullPtr)
  framebufferTexture albedoSpec GL.Framebuffer (GL.ColorAttachment 2) GL.Texture2D  0

  GL.drawBuffers $=! [ GL.FBOColorAttachment 0
                     , GL.FBOColorAttachment 1
                     , GL.FBOColorAttachment 2]

  depthBuffer <- GL.genObjectName
  GL.bindRenderbuffer GL.Renderbuffer $=! depthBuffer

  liftIO $ GL.renderbufferStorage GL.Renderbuffer GL.DepthComponent'  (GL.RenderbufferSize (fromIntegral width) (fromIntegral height))
  liftIO $ GL.framebufferRenderbuffer Framebuffer DepthAttachment GL.Renderbuffer depthBuffer

  fbStatus <- get (GL.framebufferStatus Framebuffer)
  GL.bindFramebuffer Framebuffer $=! GL.defaultFramebufferObject

  when (fbStatus /= GL.Complete) $ error (show fbStatus)
  pure GBuffer
      { _renderWidth       = width
      , _renderHeight      = height
      , _albedoSpecTexture = albedoSpec
      , _normalTexture     = normals
      , _positionTexture   = positions
      , _depthBuffer       = depthBuffer
      , _frameBuffer       = frameBuffer
      }

  where
    size = texSize2D width height
    minFilter = (Nearest, Nothing)
    magFilter = Nearest

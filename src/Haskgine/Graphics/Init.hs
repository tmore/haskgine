module Haskgine.Graphics.Init where

import Data.Map (Map)
import qualified Data.Map as Map

import qualified Graphics.Rendering.OpenGL as GL


import Haskgine.Util.GL.VAO

data GraphicsData = GraphicsData
                    { programs :: Map String GL.Program
                    , vaos     :: Map String VAO
                    } deriving (Show, Eq)

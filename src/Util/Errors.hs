module Util.Errors where


import Control.Monad.Except


liftError :: (Monad m, MonadError err m) => (err' -> err) -> m (Either err' a) -> m a
liftError f action = do
  e <- action
  liftEither $ case e of
    Left err -> Left (f err)
    (Right a) -> Right a

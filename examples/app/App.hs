-- | Author: Tomas Möre 2019
{-# LANGUAGE OverloadedStrings, FlexibleContexts, LambdaCase, RecordWildCards, TupleSections #-}
module App where

import Haskgine.Reflex
import Haskgine.Reflex.Util.Entity
import Haskgine.Reflex.Util.Time
import Haskgine.Reflex.Util.Input.Mouse
import Haskgine.Reflex.Util.Input.Keyboard
import Haskgine.Reflex.Util.Window
import Haskgine.Reflex.Util.Camera

import Haskgine.Reflex.Util.Misc

import Haskgine.Graphics.Camera as Camera
import Haskgine.Util.Linalg

import Data.Default

import Reflex.Network

import Control.Monad
app :: (ReflexHaskgine r t m) => m (RenderControlls t)
app = do
  poe <- askPostRenderEvent

  timePassed <- secondsSinceStart

  mouseLoc <- askMouseMoveEvent

  floor <- newColoredUnitQuad (1,1,1) (\ e -> e `rotateVertical` (pi/2) `withPosition` vec3 0 0 (-3) `withScale` vec3 10 10 10)
  -- updateEntityOn floor timePassed
  --   (\ t e -> e `withPosition` vec3 0 0 (0 * (sin t/10)))

  centerCube <- newColoredUnitCube (1,0,0) (`withScale` vec3 0.5 0.5 0.5)
  leftPos <- foldDyn (\ _ p -> p + 1) 0 =<< (specificKeyPressEvent Key'Q)
  rightPos <- foldDyn (\ _ p -> p - 1) 0 =<< (specificKeyPressEvent Key'E)

  let centerPositon = (+) <$> leftPos <*> rightPos
  ccPos <- updateEntityOn centerCube (updated centerPositon)
     (\ pos e -> e `withPosition` vec3 pos 0 0)

  countDyn <- eventCounter =<< specificKeyPressEvent Key'Space

  networkView $ ffor countDyn $ \max -> do
    forM_ [1.. (fromInteger max) + 10] $ \ n -> do
      let rotVec  = rotateVec (vec3 0 0 1) (pi * n/50) (vec3 1 0 0)
      orbitCube <- newColoredUnitCube (1,0,0) (\e -> e`withScale` vec3 0.1 0.1 0.1  `withPosition` rotateVec rotVec (n / 2 ) (vec3 0 0 2))

      updateEntityOn orbitCube (attachPromptlyDyn ccPos timePassed)
        (\ (parent, t) e -> e `withPosition` rotateVec rotVec (n / 2 + t / 2) (vec3 0 0 2) `addPositon` position parent)
    pure max

  frameBufferSize <- directlyUpdatedFramebuffer 1920 1080
--  clearColor <- hold (vec3 0 0 0) $ fmap (\(x,y) -> vec3 (realToFrac x) (realToFrac y) 0) mouseLoc
  positionedCamera <- makeFPSCamera
  let camera = (\ (w,h) cam -> cam `withAspect` (fromIntegral w / fromIntegral h) `withFov` (pi/2)) <$>
               frameBufferSize <*>
               current positionedCamera

  handleCursorDisabled =<< specificKeyToggleDyn Key'Escape False

  keepLooping <- hold False . (True <$) =<<  onCharCombo ('q':|"uit")
  pure $ MKRenderControlls camera (constant (vec3 0 0 0)) frameBufferSize keepLooping

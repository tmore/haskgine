{-# LANGUAGE OverloadedStrings #-}

module Main where

import App

import Haskgine.Reflex.Core

import Haskgine.Renderer (Renderer)
import Haskgine.Renderer.DeferredRenderer

import Haskgine.Graphics.Camera as Camera
import Haskgine.Util.Linalg.Transform (withPosition)

import qualified Graphics.UI.GLFW as GLFW
import qualified Graphics.Rendering.OpenGL as GL

import Numeric.LinearAlgebra.Static (vec3)

import Data.Default
import Data.Maybe
import Data.StateVar

import Control.Monad.IO.Class
import Control.Monad.Except


loadRenderer :: MonadIO m => ExceptT String m DeferredRenderer
loadRenderer = do
  programs <- makeDeferredPrograms files
  makeDeferredRenderer programs 1920 1080 1024 1024
  where
    files = DeferredRendererShaderFiles
            { _shadowVertFp = "shaders/shadow.vert"
            , _shadowFragFp = "shaders/shadow.frag"

            , _entityVertFp = "shaders/g_buf.vert"
            , _entityFragFp = "shaders/g_buf.frag"

            , _finalizeVertFp = "shaders/finalize.vert"
            , _finalizeFragFp = "shaders/finalize.frag"
            }

main :: IO ()
main = do
  maybeWindow <- initGLFW initialWindowSize
  eRenderer <- runExceptT loadRenderer
  case maybeWindow of
    Just window -> do
      case eRenderer of
        Left errorMsg -> putStrLn errorMsg
        Right renderer ->
          host window renderer app


    Nothing -> putStrLn "Something went wrong when creating a window"
  where
    initialWindowSize = (1920, 1080)


initGLFW :: (Int, Int) -> IO (Maybe GLFW.Window)
initGLFW (windowWidth, windowHeight) = do
  initSuccess <- GLFW.init
  unless initSuccess (putStrLn "Couldn't init glfw")
  GLFW.swapInterval 1
  GLFW.windowHint $ GLFW.WindowHint'Samples (Just 2)
  GLFW.windowHint $ GLFW.WindowHint'ContextVersionMajor 3
  GLFW.windowHint $ GLFW.WindowHint'ContextVersionMinor 3
  GLFW.windowHint $ GLFW.WindowHint'OpenGLForwardCompat True
  GLFW.windowHint $ GLFW.WindowHint'RefreshRate (Just 60)
  GLFW.windowHint $ GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core
  GLFW.windowHint $ GLFW.WindowHint'Resizable True
  GLFW.windowHint $ GLFW.WindowHint'DepthBits (Just maxBound)

  GLFW.setErrorCallback $ Just (\ err str -> do
                                   print ("GLFW GL ERROR: ", str, err)
                             )

  window <- GLFW.createWindow windowWidth windowHeight "Test window" Nothing Nothing
  when (isNothing window) (putStrLn "Couldn't create window")

  GLFW.makeContextCurrent window
  return window

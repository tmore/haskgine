-- | Author: Tomas Möre 2019
{-# LANGUAGE OverloadedStrings, FlexibleContexts, LambdaCase, RecordWildCards, TupleSections, RecursiveDo, MultiWayIf, DataKinds, DuplicateRecordFields  #-}
module Pong where

import Haskgine.Reflex
    ( vec3,
      attach,
      ffilter,
      gate,
      foldDyn,
      Key(Key'Escape, Key'F, Key'V, Key'Up, Key'Down),
      HasHaskgineCore(askMouseMoveEvent, askPostRenderEvent),
      ConcreteReflexHaskgine,
      RenderControlls(MKRenderControlls),
      MonadHold(hold),
      Reflex(constant, Event, updated, current),
      Global,
      Spider,
      SpiderTimeline, PerformEvent (performEvent_) )

import Haskgine.Reflex.Util.Entity
import Haskgine.Reflex.Util.Time
import Haskgine.Reflex.Util.Input.Mouse
import Haskgine.Reflex.Util.Input.Keyboard
import Haskgine.Reflex.Util.Window
import Haskgine.Reflex.Util.Camera

import Haskgine.ImGui

import Haskgine.Reflex.Core.Host ( ConcreteReflexHaskgine )
import Haskgine.Reflex.Util.Misc

import Haskgine.Graphics.Camera as Camera
import Haskgine.Util.Linalg

import Numeric.LinearAlgebra.Static ((#>))

import Haskgine.Util.Math

import Haskgine.Util.Math.Geometry 

import Debug.Trace

import Data.Default
import Data.Align
import Data.These

import Reflex.Network

import System.Random

import Control.Monad
import Control.Monad.IO.Class

-- app :: (ReflexHaskgine r t m) => m (RenderControlls t)
pong :: ConcreteReflexHaskgine (RenderControlls Spider)
pong = do
  poe <- askPostRenderEvent

  timePassed <- secondsSinceStart
  deltaTick <- deltaTickEvent
  mouseLoc <- askMouseMoveEvent

  withWindow "Hello world!" $ do
    text "Muh text"
    clickedEvt <- button "Click meh"
    performEvent_ (fmap (const (liftIO $ putStrLn "Clicked")) clickedEvt)
  withWindow "Hello world!2" $ do
      text "Muh other window"

  let playMoveSpeed = 0.03
      playerScale = vec3 0.05 0.3 0.1
      playerPosEvent upKey downKey = do
       w_heldB <- specificKeyHeldBehavior upKey
       s_heldB <- specificKeyHeldBehavior downKey
       let moveUp = void $ gate w_heldB deltaTick
           moveDown = void $ gate s_heldB deltaTick

           move = ffilter (\case
                              (That d) -> True
                              (This d) -> True
                              (These a b) -> False
                          )  (align moveUp moveDown)
       foldDyn (\ theseDir pos ->
                  let dir = case theseDir of
                                This a -> 1
                                That a -> -1
                                These a b -> 0
                  in clamp (-1 + dimY playerScale) (1 - dimY playerScale) (pos + playMoveSpeed * dir)
                  ) (0 :: Double) move



  leftPlayer <- newColoredUnitCube (1,0,0) (\ e -> e `withScale` playerScale `withPosition` vec3 (-1) 0 0 )
  leftPlayerPos <- fmap (clamp (-1) 1) <$> playerPosEvent Key'F Key'V
  let leftPlayerAABB = fmap (\ posY -> MkAABB playerScale (vec3 (-1) posY 0) ) leftPlayerPos

  rightPlayer <- newColoredUnitCube (1,0,0) (\ e -> e `withScale` playerScale `withPosition` vec3 1 0 0 )
  rightPlayerPos <- fmap (clamp (-1) 1) <$> playerPosEvent Key'Up Key'Down
  let rightPlayerAABB = fmap (\ posY -> MkAABB playerScale (vec3 1 posY 0) ) rightPlayerPos

  let ballScale = vec3 0.075 0.075 0.075
      ballStartVector = normalize $ vec3 1 0.1 0
  ball <- newColoredUnitCube (1,1,1) (\ e -> e `withScale` ballScale `withPosition` vec3 0 0 0 )

  let ballSpeed = 1.5
      ballMoveEvent :: Event (SpiderTimeline Global) (AABB, (AABB, Double))
      ballMoveEvent = attach (current leftPlayerAABB) (attach (current rightPlayerAABB) deltaTick)

      
  ballDecXballPos <- foldDyn (\ (lPos, (rPos, deltaT))  (vector, pos) -> ballUpdateLogic lPos rPos deltaT ballSpeed vector pos )
                            (ballStartVector, MkAABB ballScale (vec3 0 0 0)) ballMoveEvent
  let ballPosD = fmap snd ballDecXballPos

      --colidesWithLeft = intersects lblock nextBall
  _ <- updateEntityOn leftPlayer (updated leftPlayerAABB)
                      (\ MkAABB{..} entity -> entity `withPosition` _position)
  _ <- updateEntityOn rightPlayer (updated rightPlayerAABB)
                      (\ MkAABB{..} entity -> entity `withPosition` _position)

  -- playfield frame
  let frameColor = (0.5,0.5,0.5)
      frameThickness = 0.05
  -- Ceiling
  newColoredUnitCube frameColor (\ e -> e `withScale` vec3 2.4 frameThickness 1
                                        `withPosition` vec3 0 (1 + frameThickness) 0 )
  -- floor
  newColoredUnitCube frameColor (\ e -> e `withScale` vec3 2.4 frameThickness 1
                                        `withPosition` vec3 0 (-1 - frameThickness) 0 )
  -- left wall
  newColoredUnitCube frameColor (\ e -> e `withScale` vec3 frameThickness 2 1
                                        `withPosition` vec3 (-1.2 - frameThickness) 0 0 )
  -- right wall
  newColoredUnitCube frameColor (\ e -> e `withScale` vec3 frameThickness 2 1
                                        `withPosition` vec3 (1.2 + frameThickness) 0 0 )
  
  
  ballC <- updateEntityOn ball (updated ballPosD)
      (\ MkAABB{..} e -> e `withPosition` _position )

  frameBufferSize <- directlyUpdatedFramebuffer 1920 1080
--  clearColor <- hold (vec3 0 0 0) $ fmap (\(x,y) -> vec3 (realToFrac x) (realToFrac y) 0) mouseLoc
  positionedCamera <- makeFPSCamera (def `withCameraPosition` vec3 0 0 1) deltaTick

  let camera = (\ (w,h) cam -> cam `withAspect` (fromIntegral w / fromIntegral h) `withFov` (pi/2) ) <$>
               frameBufferSize <*>
               current positionedCamera

  handleCursorDisabled =<< specificKeyToggleDyn Key'Escape False

  keepLooping <- hold False . (True <$) =<<  onCharCombo ('q':|"uit")
  pure $ MKRenderControlls camera (constant (vec3 0 0 0)) frameBufferSize keepLooping (constant (Just def))

 
ballUpdateLogic :: AABB -> AABB -> Double -> Double ->  R 3 -> AABB -> (R 3 , AABB)
ballUpdateLogic lblock rblock deltaT ballSpeed vector ball@(MkAABB ballScale ballPos)
  | isOutsideFieldX = ( normalize invertedVector , MkAABB ballScale (vec3 0 0 0))
  | playerBounce = ( traceShow rotation (normalize (rotateMatrix3 (vec3 0 0 1) rotation #> invertedVector)) , MkAABB ballScale (ballPos + vecScalarMultiply (deltaT * ballSpeed) invertedVector) )
  | roofBounce = ( normalize roofInvertedVector , MkAABB ballScale (ballPos + vecScalarMultiply (deltaT * ballSpeed) roofInvertedVector))
  | otherwise = (vector, nextBall )
  where
     nextPos = vecScalarMultiply (deltaT * ballSpeed) vector + ballPos
     nextBall = MkAABB ballScale nextPos
     nextPosX = dimX nextPos
     nextPosY = dimY nextPos

     isOutsideFieldX = nextPosX <= (-1.2) || 1.2 <= nextPosX
     roofBounce = nextPosY <= (-1)  || nextPosY >= 1
     colidesWithLeft = intersects lblock nextBall
     colidesWithRight = intersects rblock nextBall
     playerBounce = colidesWithLeft || colidesWithRight
     invertedVector = multiplyX (-1) vector
     roofInvertedVector = multiplyY (-1) vector
     bouncedPlayer = if colidesWithLeft then lblock else rblock
     yDistDiff :: Double
     yDistDiff = (dimY ballPos - dimY (centerOf3D bouncedPlayer)) / dimY (Haskgine.Util.Math.Geometry._scale bouncedPlayer)

     rotation = 0.75 * ((if colidesWithLeft then 1 else (-1))  *
                        (pi / 2) * clamp (-1) 1 yDistDiff)


-- | >>> rotateAroundM3 (vec3 0 0 1) (pi / 2) #> vec3 1 0 0
-- WAS WAS WAS WAS (vector [1.0,-1.2246063538223773e-16,0.0] :: R 3)
-- WAS WAS WAS NOW (vector [-2.220446049250313e-16,-1.0,0.0] :: R 3)
-- WAS WAS NOW (vector [2.220446049250313e-16,1.0,0.0] :: R 3)
-- WAS NOW (vector [-2.220446049250313e-16,-1.0,0.0] :: R 3)
-- NOW (vector [2.220446049250313e-16,1.0,0.0] :: R 3)
